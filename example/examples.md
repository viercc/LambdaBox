# Explanation of sample programs

This directory (/example/) contains some sample programs of LambdaBox.
This document explains these.

## LambdaBox interpreter usage

After doing `cabal install` this project,
you can run the interpreter `lbox`.

``` console
$ lbox hello.png
Hello, world!
```

By passing `-e` (`--expr`) option, you can get to see the textual representation of the program,
instead of running it.

``` console
$ lbox -e id.png
\x -> x
```

## Complete programs

### id.png

The most small example is [id.png](id.png). It outputs its input as-is.

``` console
$ echo foobar | lbox id.png
foobar
```

### noop.png

[noop.png](noop.png) outputs nothing.

``` console
$ lbox noop.png
$
```

### hello.png

[hello.png](hello.png) is a Hello world program.

``` console
$ lbox hello.png
Hello, world!
```


### abc.png

[abc.png](abc.png) outputs 256 bytes of output `[0,1,2,...,255]`.

``` console
$ lbox abc.png > abc.output
$ hd abc.output
00000000  00 01 02 03 04 05 06 07  08 09 0a 0b 0c 0d 0e 0f  |................|
00000010  10 11 12 13 14 15 16 17  18 19 1a 1b 1c 1d 1e 1f  |................|
00000020  20 21 22 23 24 25 26 27  28 29 2a 2b 2c 2d 2e 2f  | !"#$%&'()*+,-./|
00000030  30 31 32 33 34 35 36 37  38 39 3a 3b 3c 3d 3e 3f  |0123456789:;<=>?|
00000040  40 41 42 43 44 45 46 47  48 49 4a 4b 4c 4d 4e 4f  |@ABCDEFGHIJKLMNO|
00000050  50 51 52 53 54 55 56 57  58 59 5a 5b 5c 5d 5e 5f  |PQRSTUVWXYZ[\]^_|
00000060  60 61 62 63 64 65 66 67  68 69 6a 6b 6c 6d 6e 6f  |`abcdefghijklmno|
00000070  70 71 72 73 74 75 76 77  78 79 7a 7b 7c 7d 7e 7f  |pqrstuvwxyz{|}~.|
00000080  80 81 82 83 84 85 86 87  88 89 8a 8b 8c 8d 8e 8f  |................|
00000090  90 91 92 93 94 95 96 97  98 99 9a 9b 9c 9d 9e 9f  |................|
000000a0  a0 a1 a2 a3 a4 a5 a6 a7  a8 a9 aa ab ac ad ae af  |................|
000000b0  b0 b1 b2 b3 b4 b5 b6 b7  b8 b9 ba bb bc bd be bf  |................|
000000c0  c0 c1 c2 c3 c4 c5 c6 c7  c8 c9 ca cb cc cd ce cf  |................|
000000d0  d0 d1 d2 d3 d4 d5 d6 d7  d8 d9 da db dc dd de df  |................|
000000e0  e0 e1 e2 e3 e4 e5 e6 e7  e8 e9 ea eb ec ed ee ef  |................|
000000f0  f0 f1 f2 f3 f4 f5 f6 f7  f8 f9 fa fb fc fd fe ff  |................|
00000100
```

### echo-twice.png

[echo-twice.png](echo-twice.png) outputs each input bytes twice.

``` console
$ echo Hello, world! | lbox echo-twice.png
HHeelllloo,,  wwoorrlldd!!
```

### filter-even.png

[filter-even.png](filter-even.png) outputs each even byte of input. Odd-valued
bytes are filtered out.

``` console
$ echo Hello, world! | lbox filter-even.png
Hll, rld
```

### uppercase.png

[uppercase.png](uppercase.png) outputs each byte of input, but if that is a
lowercase alphabet in ASCII code, output the uppercase version of it instead.

``` console
$ echo What do you do? | lbox uppercase.png
WHAT DO YOU DO?
```

## Not a runnable program but a function

Some of the examples are not a complete program, but merely a function
which can be copy-pasted to be a part of complete program.
[const.png](const.png) is such an example.

Running these programs probably yields an runtime error, hangs up, or shows
meaningless outputs. It's because these does not follows the convention of main
function: takes infinite list of numbers and returns infinite list of numbers.

You can show the expression of these programs no problem.

``` console
$ lbox -e const.png
\x2 x -> x2
```

These are not-a-program functions:

* [const.png](const.png)
* [apply.png](apply.png)
* [revap.png](revap.png)
* [succ.png](succ.png)
* [mult.png](mult.png)
* [mult3.png](mult3.png)

## Incomplete programs

The interpreter has a REPL feature for development and debugging.
Using REPL, you can test much more complex functions than simply looking at
parsed expression of them.

Example usages of REPL:

``` console
$ lbox -i
> "Hello" :: ByteStream
48 65 6C 6C 6F
> (\f -> 3 (5 f)) :: Int
15
> :l succ = "succ.png"
> :p succ
\x2 x1 x -> x1 (x2 x1 x)

> succ 30 :: Int
31
```

* Input `<expression> :: <type>` to interpret `<expression>` as `<type>`.
  For example,
  
  ```
  > \f x -> f (f x) :: Int
  2
  > \x y -> y :: Bool
  $F
  ```
  
  `<type>` can be: `Int`, `Bool`, `Char`, `ByteStream`,
  `{t}` which means (Scott encoded) infinite list of `t`,
  `[t]` which means (Scott encoded) list of `t`.

* Input `:l <VarName> = "<FILE>"` to load a LambdaBox source file (an image)
  to variable `<VarName>`.
* Input `:d <VarName> = <Expr>` to define `<VarName>` with definition `<Expr>`.
* Input `:p <VarName>` to print the definition of `<VarName>`.
* Input `:help` to print brief explanation of usage.
* Input `:quit` to exit the REPL.

### succ.png

[succ.png](succ.png) is a successor function of church-encoded numerals.


``` console
$ lbox -i
> :l succ = "succ.png"
> succ 30 :: Int
31
```

### add.png

[add.png](add.png) adds two church-encoded numerals.

``` console
$ lbox -i
> :l add = "add.png"
> add 30 3 :: Int
33
```

### sub.png

[sub.png](sub.png) subtracts church-encoded numerals.

``` console
$ lbox -i
> :l sub = "sub.png"
> sub 100 256 :: Int
0
> sub 1000 256 :: Int
744
> sub 10000 256 :: Int
9744
```

### subfast.png

[subfast.png](subfast.png) subtracts church-encoded numerals.
It is a bit complex than sub.png but runs faster than it.

``` console
$ lbox -i
> :l sub = "subfast.png"
> sub 100 256 :: Int
0
> sub 1000 256 :: Int
744
> sub 10000 256 :: Int
9744
```

### div10.png

[div10.png](div10.png) divides a church-encoded numerals by 10.

``` console
$ lbox -i
> :l div10 = "div10.png"
> div10 1000 :: Int
100
> div10 10000 :: Int
1000
```

### isprime.png

[isprime.png](isprime.png) takes a church-encoded numeral. If it is greater than 1 and is a prime number, return 1. if it is a composite number, return 0. For input 0 or 1, it returns **1**. That is wrong, but I didn't bother to fix it by adding input range checks.

``` console
$ lbox -i
> :l isprime = "isprime.png"
> isprime 103 :: Int
1
> isprime 10007 :: Int
1
> isprime 10001 :: Int
0
> isprime 0 :: Int
1
```

### show-nat.png

[show-nat.png](show-nat.png) takes a church-encoded numeral, and converts it to a text.

``` console
$ lbox -i
> :l showNat = "show-nat.png"
> showNat 1234 :: ByteStream
31 32 33 34
```

### read-nat.png

[read-nat.png](read-nat.png) parses input text to a church-encoded numeral. Parsing stops at first non-digit character, and returns 0 when there's no leading digit characters.

``` console
$ lbox -i
> :l readNat = "read-nat.png"
> readNat "1234" :: Int
1234
> readNat "1234foobar" :: Int
1234
> readNat "foobar1234" :: Int
0
```
