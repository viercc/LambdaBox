# LambdaBox

LambdaBox is a very simple and primitive graphical programming language
which is pure and functional.

In LambdaBox, you write an expression of lambda calculus extended with `letrec`, in an unusual way.
You write that in the form of a *bitmap image*!

For example, the following image is a "hello world" program in LambdaBox.

![example/hello.png](/example/hello.png)

Running it with LambdaBox interpreter, it outputs "Hello, world!" to the stdout.
(`lbox` is the name of LambdaBox interpreter binary.)

```shell-session
$ lbox example/hello.png
Hello, world!
```

To know the language, see [Language Guide](LanguageGuide.md).

## How to build & install

### Installing prerequisite tools

LambdaBox is written in Haskell, with very liberal use of the Glasgow Haskell Compiler (GHC)
extensions. It is not intended to be usable on other Haskell implementations.

Use [ghc](https://www.haskell.org/ghc/) and [cabal-install](https://cabal.readthedocs.io/en/stable/) to build and install LambdaBox.
To install them, the recommended method is using [ghcup](https://www.haskell.org/ghcup/).

If you use `ghcup`, the following commands install the required versions of `ghc` and `cabal-install`.

```shell-session
$ ghcup install ghc 9.6.2
$ ghcup install cabal 3.10.1.0
```

### Building & installing

`cabal` command will do all the required work. On the root directory of this project, run `cabal build` to build the executables:

```shell-session
$ cabal build lbox # Build executables
```

And `cabal install` will put the compiled executable to somewhere, usually `~/.cabal/bin/`.

```shell-session
$ cabal install lbox # Puts the executables to the "install directory" designated by cabal-install 
```

### Running without installing

You can run LambdaBox binary without installing it somewhere, by `cabal run` command.

```shell-session
$ cabal run -- lbox example/hello.png
```

See [/example/examples.md](/example/examples.md) to look up how to run example programs.
