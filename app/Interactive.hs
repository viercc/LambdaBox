module Interactive (Repl, runRepl, replLoop) where

-- import           Data.Lambda.Core             (makeLetRec)

import Codec.Picture.Types
import Control.Applicative
import Control.Monad.Catch
import Control.Monad.State
import Data.Char (chr)
import qualified Data.Lambda.Parse as P
import Data.List (intersperse)
import qualified Data.Map.Lazy as Map
import LambdaBox.Untyped.Expr
import Geom.Rect
import LambdaBox.Image
import LambdaBox.Untyped.ImageParser
import LambdaBox.Untyped.Interpreter
import qualified LambdaBox.Untyped.Parse as P
import Optimization
import System.Console.Haskeline hiding (outputStrLn)
import qualified System.Console.Haskeline as Haskeline
import qualified Text.Megaparsec as P
import Text.PrettyPrint.Formattable
import Text.Printf
import Data.Bool (bool)
import qualified Data.ByteString.Lazy as BS


-- * Command definitions

data ReplCommand
  = Load VarName FilePath ClipOption ShrinkOption
  | Def VarName LBExpr
  | PrintExpr VarName
  | Eval LBExpr InterpretType
  | Help
  | Quit

type ClipOption = Maybe Rect

data InterpretType
  = AsInt
  | AsBool
  | AsChar
  | AsByteStream
  | AsStreamOf InterpretType
  | AsListOf InterpretType
  deriving (Show, Read)

-- * Command parsing

commandP :: P.Parser ReplCommand
commandP =
  Load
    <$ P.symbol ":l"
    <*> P.varP
    <* P.symbol "="
    <*> P.stringLiteral
    <*> clipP
    <*> shrinkP
    <|> Def <$ P.symbol ":d" <*> P.varP <* P.symbol "=" <*> P.lamP'
    <|> PrintExpr <$ P.symbol ":p" <*> P.varP
    <|> Help <$ P.symbol ":help"
    <|> Quit <$ P.symbol ":quit"
    <|> Eval <$> P.lamP' <* P.symbol "::" <*> interpretTypeP

interpretTypeP :: P.Parser InterpretType
interpretTypeP =
  AsInt <$ P.symbol "Int"
    <|> AsBool <$ P.symbol "Bool"
    <|> AsChar <$ P.symbol "Char"
    <|> AsByteStream <$ P.symbol "ByteStream"
    <|> AsStreamOf <$> braces interpretTypeP
    <|> AsListOf <$> brackets interpretTypeP
  where
    braces = P.between (P.symbol "{") (P.symbol "}")
    brackets = P.between (P.symbol "[") (P.symbol "]")

clipP :: P.Parser ClipOption
clipP = P.optional $ brackets clipP'
  where
    brackets = P.between (P.symbol "[") (P.symbol "]")
    clipP' =
      Rect
        <$> P.intLiteral
        <* P.symbol ","
        <*> P.intLiteral
        <* P.symbol ":"
        <*> P.intLiteral
        <* P.symbol ","
        <*> P.intLiteral

shrinkP :: P.Parser ShrinkOption
shrinkP = ShrinkOption <$> P.option 2 shrinkP'
  where
    shrinkP' =
      2 <$ P.symbol "-shrink"
        <|> 0 <$ P.symbol "-no-shrink"

-- * Command-running monad

type ReplState = Map.Map VarName LBExpr

type Repl = StateT ReplState (InputT IO)

initialState :: ReplState
initialState = Map.empty

runRepl :: Repl () -> IO ()
runRepl repl = runInputT defaultSettings (evalStateT repl initialState)

outputStrLn :: String -> Repl ()
outputStrLn = lift . Haskeline.outputStrLn

prompt :: String
prompt = "> "

interruptably :: Repl () -> Repl ()
interruptably (StateT m) =
  StateT $ \st ->
    let abortCase =
          Haskeline.outputStrLn "Interrupted"
            >> return ((), st)
     in withInterrupt $
          handleInterrupt abortCase (m st)

-- * Repl Logic

replLoop :: Repl ()
replLoop =
  do
    mline <- lift (getInputLine prompt)
    case mline of
      Nothing -> replLoop
      Just line -> case parseCommand line of
        Left errMsg ->
          do
            outputStrLn "Parse error on command:"
            outputStrLn (P.errorBundlePretty errMsg)
            replLoop
        Right Quit -> return ()
        Right cmd -> interruptably (handleCommand cmd) >> replLoop

parseCommand :: String -> Either (P.ParseErrorBundle String Void) ReplCommand
parseCommand = P.parse (commandP <* P.eof) "UserInput"

handleCommand :: ReplCommand -> Repl ()
handleCommand cmd = case cmd of
  Load x filePath clipOpt shrinkOpt ->
    do
      readResult <-
        lift $
          handle (return . Left . showException) $
            liftIO $
              readImageRGBA8 filePath
      case readResult of
        Left err -> outputStrLn err
        Right img ->
          let img' = maybe id clip clipOpt img
              parseResult = parseFromRGBA img' filePath
           in case parseResult >>= checkError of
                Left errors -> outputStrLn $ format errors
                Right expr ->
                  let expr' = getOptimizer shrinkOpt expr
                   in modify' (Map.insert x expr')
  Def x expr -> modify' (Map.insert x expr)
  PrintExpr x ->
    do
      mayExpr <- gets (Map.lookup x)
      case mayExpr of
        Nothing -> outputStrLn $ show x ++ "is not defined"
        Just e -> outputStrLn $ formatLam e
  Eval expr ty ->
    do
      defs <- get
      let allExpr = makeLetRec (Map.toList defs) expr
      val <- liftIO $ eval allExpr
      lift . handle (Haskeline.outputStrLn . showException) $
        do
          printAsTy val ty
          Haskeline.outputStrLn ""
  Help -> do
    outputStrLn "Command syntax:"
    outputStrLn "  :l VarName = \"FILE\" [CLIP] [SHRINK]"
    outputStrLn "     CLIP := [x1,y1 : x2,y2]"
    outputStrLn "     SHRINK := -shrink | -no-shrink"
    outputStrLn "  :d VarName = Expr"
    outputStrLn "  :p VarName"
    outputStrLn "  :help"
    outputStrLn "  :quit"
    outputStrLn "  Expr :: Type"
    outputStrLn "    Type := Int | Bool | Char | ByteStream | {Type} | [Type]"
  Quit -> return ()

printAsTy :: LBData -> InterpretType -> InputT IO ()
printAsTy x ty = liftIO (showAs ty x) >>= Haskeline.outputStr

showAs :: InterpretType -> LBData -> IO String
showAs AsInt x = show <$> decodeInt x
showAs AsBool x = bool "$T" "$F" <$> decodeBool x
showAs AsChar x = show . chr <$> decodeInt x
showAs AsByteStream x = showByteStream . fst <$> decodeToByteStream x
showAs (AsStreamOf ty) x = showListOf <$> getContentsOfStreamIO (decodeToStreamIO (showAs ty) x)
showAs (AsListOf ty) x = showListOf <$> decodeToList (showAs ty) x

showByteStream :: BS.ByteString -> String
showByteStream xs = BS.unpack xs >>= printf "%02X "

showListOf :: [String] -> String
showListOf xs = concat $ "[" : (intersperse ", " xs ++ ["]"])

showException :: SomeException -> String
showException = show

clip :: Rect -> LBImage -> LBImage
clip r@(Rect x1 y1 _ _) img =
  let f x y = pixelAt img (x1 + x) (y1 + y)
   in generateImage f (rectWidth r) (rectHeight r)
