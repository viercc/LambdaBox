module Main where

import Data.Either (partitionEithers)
import Interactive
import LambdaBox.Error
import LambdaBox.Untyped.Expr
import LambdaBox.Untyped.ImageParser
import LambdaBox.Untyped.Interpreter
import Optimization
import Options.Applicative
import Text.PrettyPrint.Formattable
import Prelude
import System.Exit (exitWith)

data Option = Option
  { flagExpr :: Bool,
    flagInteractive :: Bool,
    shrinkOpts :: ShrinkOption,
    flagNoCheck :: Bool,
    flagNewIO :: Bool,
    files :: [String]
  }
  deriving (Show)

optionP :: Parser Option
optionP =
  Option
    <$> flagExprP
    <*> flagInteractiveP
    <*> shrinkOptionP
    <*> flagNoCheckP
    <*> flagNewIOP
    <*> filesP
  where
    flagExprP =
      switch $
        long "expr"
          <> short 'e'
          <> help "Do not run. Only parse to lambda expression and print it."

    flagInteractiveP =
      switch $
        long "interactive"
          <> short 'i'
          <> help "Goes to interactive mode. Ignores all other options."

    flagNoCheckP =
      switch $
        long "no-check"
          <> help "Do not check some errors during parsing."

    flagNewIOP =
      switch $
        long "new-io"
         <> help "Evaluate using new (exprimental) IO representaion"
    
    filesP =
      many $
        strArgument $
          metavar "IMGFILE..."
            <> help "LambdaBox source files."

shrinkOptionP :: Parser ShrinkOption
shrinkOptionP = ShrinkOption <$> option auto mods
  where
    mods =
      long "shrink"
        <> short 's'
        <> metavar "LEVEL"
        <> value 2
        <> help "Optimization level as int value between 0-3"

parseOption :: IO Option
parseOption = execParser opts
  where
    opts = info (helper <*> optionP) fullDesc

getChecker :: Option -> LBExpr -> Either [LBError] LBExpr
getChecker opt = if flagNoCheck opt then return else checkError

main :: IO ()
main = do
  opt <- parseOption
  if flagInteractive opt
    then runRepl replLoop
    else
      if null (files opt)
        then putStrLn "Specify one or more source files"
        else interpreterMain opt

interpreterMain :: Option -> IO ()
interpreterMain opt = do
  let checker = getChecker opt
      optimizer = getOptimizer (shrinkOpts opt)
      evalMethod
        | flagNewIO opt = runProgramTree . decodeProgramTree
        | otherwise     = runMain
  
  parseResult <- mapM parseFile (files opt)
  let checkResult = fmap (>>= checker) parseResult
      (errorss, exprs) = partitionEithers checkResult
  if null errorss
    then
      let expr = optimizer $ foldl1 app_ exprs
       in if flagExpr opt
            then putStrLn (formatLam expr)
            else eval expr >>= evalMethod >>= exitWith
    else do
      putStrLn "Parse Error"
      putStrLn $ format (concat errorss)
