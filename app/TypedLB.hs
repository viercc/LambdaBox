{-# LANGUAGE MultiWayIf #-}
module Main (main) where

import Data.List (intercalate)
import Data.Void (absurd, Void, vacuous)
import Control.Monad ((>=>), when)
import Data.Bifunctor (Bifunctor(..))
import Data.Traversable (for)
import Data.Maybe (fromMaybe)

import System.Exit (exitFailure)
import System.IO (hPutStrLn, stderr)

import Options.Applicative hiding (str)

import Text.PrettyPrint.Formattable
import Data.Lambda.Format (formatLam)

import qualified LambdaBox.Typed.Expr.DeBruijn as DB
import LambdaBox.Typed.ImageParser
import LambdaBox.Typed.ImageParser.Expr (VarName)
import qualified LambdaBox.Typed.Eval.Types as Eval
import qualified LambdaBox.Typed.Expr.Named as Named
import LambdaBox.Error (LBError(..), noPosition)

import qualified Data.VarName as CommonVarName
import LambdaBox.Untyped.Expr (LBExpr)
import LambdaBox.Untyped.Interpreter (eval, decodeInt, decodeBool)


data Mode = Parse | TypeCheck | Eval
  deriving (Eq, Ord, Enum, Bounded, Show, Read)

data Option = Option
  { mode :: Mode,
    verbosity :: Maybe Verbosity,
    fSimplifies :: Bool,
    fPrintNamed :: Bool,
    dDebugPrint :: Bool,
    srcFiles :: [FilePath]
  }
  deriving (Show)

type Verbosity = Int

autoMetaVar :: (Show a, Enum a, Bounded a) => a -> String
autoMetaVar a = "<" ++ intercalate "|" [show (x `asTypeOf` a) | x <- [minBound .. maxBound]] ++ ">"

longSwitch :: Bool -> String -> Mod FlagFields Bool -> Parser Bool
longSwitch defaultValue switchName otherMod =
  flag' True (long switchName <> otherMod)
    <|> flag' False (long ("no-" ++ switchName) <> otherMod <> help "-")
    <|> pure defaultValue

optionP :: Parser Option
optionP =
  Option
    <$> modeP
    <*> verbosityP
    <*> longSwitch True "simpl" (help "Preforms simplification")
    <*> longSwitch True "named" (help "Prints named expression")
    <*> longSwitch False "debug" (help "Prints all passes")
    <*> filesP
  where
    modeP =
      option auto $
        long "mode"
          <> short 'm'
          <> value Parse
          <> metavar (autoMetaVar Parse)
          <> help "Default: Parse"

    verbosityP =
      option (Just <$> auto) $
        long "verbosity"
          <> short 'v'
          <> value Nothing
          <> metavar "N"

    filesP =
      some $
        strArgument $
          metavar "IMGFILE..."
            <> help "LambdaBox source files"

main :: IO ()
main = do
  opt <- parseOption
  case mode opt of
    Parse -> mainParse opt
    TypeCheck -> mainTypeCheck opt
    Eval -> mainEval opt

parseOption :: IO Option
parseOption = execParser opts
  where
    opts = info (helper <*> optionP) fullDesc

getVerbosity :: Option -> Verbosity
getVerbosity opt = fromMaybe (defaultVerbosity (mode opt)) (verbosity opt)
  where
    defaultVerbosity m = case m of
      Parse -> 1
      TypeCheck -> 1
      Eval -> 0

putError :: String -> IO ()
putError = hPutStrLn stderr

message :: Option -> String -> IO ()
message opt str = when (getVerbosity opt >= 1) $ putStrLn str

inform :: Option -> String -> IO ()
inform opt str = when (getVerbosity opt >= 2) $ putStrLn str

mainParse :: Option -> IO ()
mainParse opt = do
  hasErrors <- for (srcFiles opt) $ \src -> do
    result <- parseWith opt src
    case result of
      Left err -> putError (format err) >> pure True
      Right _ -> pure False
  when (or hasErrors) exitFailure

mainTypeCheck :: Option -> IO ()
mainTypeCheck opt = do
  hasErrors <- for (srcFiles opt) $ \src -> do
    result <- parseWith opt src
    ans <- case result of
      Left err -> putError (format err) >> pure True
      Right expr -> case DB.typeCheck expr of
        Nothing -> putError "Typecheck failed" >> pure True
        Just t -> do
          message opt "--[type]-----------------------"
          putStrLn (fmtTyWith opt t)
          pure False
    message opt ""
    pure ans
  when (or hasErrors) exitFailure

mainEval :: Option -> IO ()
mainEval opt = do
  exprTypeChecked <- for (srcFiles opt) $ \src -> do
    result <- parseWith opt src
    result' <- case result of
      Left err -> putError (format err) >> pure Nothing
      Right expr -> case DB.typeCheck expr of
        Nothing -> putError "Typecheck failed" >> pure Nothing
        Just t -> do
          message opt "--[type]-----------------------"
          message opt (fmtTyWith opt t)
          pure (Just expr)
    message opt ""
    pure result'
  case sequenceA exprTypeChecked of
    Nothing -> exitFailure
    Just es -> evalCombinedExpr opt (foldl1 DB.App es)

evalCombinedExpr :: Option -> Expr -> IO ()
evalCombinedExpr opt expr = do
  message opt "Eval"
  message opt "===="
  inform opt "--[expr(combined)]--------"
  inform opt $ fmtWith opt expr
  evaluator <- case DB.typeCheck expr of
    Nothing -> putError "Typecheck failed" >> pure (const (pure ()))
    Just t -> do
      inform opt "--[type(combined)]--------"
      inform opt $ fmtTyWith opt t
      case judgeDecodableType t of
        Nothing -> do
          putError $ "Unsupported type: " ++ fmtTyWith opt (absurd <$> t)
          pure (const (pure ()))
        Just decType -> pure (evaluatorFor decType)
  let untypedExpr = toUntyped expr
  inform opt "--[untyped_expr]--------"
  inform opt $ formatLam untypedExpr
  evaluator untypedExpr

type NamedExpr = Named.Term VarName VarName
type Expr = DB.Term VarName VarName Void Void
type Type = DB.Type VarName Void

parseWith :: Option -> FilePath -> IO (Either [LBError] Expr)
parseWith opt src = do
  message opt src
  message opt  ('=' <$ src)
  resultNamed <- if dDebugPrint opt then parseFileDebug src else parseFile src
  let result = resultNamed >>= fromNamed
  for result $ \expr -> do
    inform opt "--[expr(pre-simpl)]------------"
    inform opt $ fmtWith opt expr
    let expr' = simplWith opt expr
    message opt "--[expr]-----------------------"
    message opt $ fmtWith opt expr'
    pure expr'

simplWith :: Option -> Expr -> Expr
simplWith opt = if fSimplifies opt then DB.simpl else id

fromNamed :: NamedExpr -> Either [LBError] Expr
fromNamed expr = first toError $ DB.toClosedTerm (DB.fromNamed expr)
  where
    toError (Left a) = pure $ LBError noPosition $ "Undefined tyvar: " ++ format a
    toError (Right a) = pure $ LBError noPosition $ "Undefined var: " ++ format a

toUntyped :: Expr -> LBExpr
toUntyped expr = vacuous $ DB.toUntyped $ DB.mapX (CommonVarName.VarName . format) expr

fmtWith :: Option -> Expr -> String
fmtWith opt = if fPrintNamed opt then format . DB.toNamed . bimap absurd absurd else format

fmtTyWith :: Option -> Type -> String
fmtTyWith opt = if fPrintNamed opt then format . DB.toNamedTy . vacuous else format

data DecodableType = DecodeToBool | DecodeToInt

judgeDecodableType :: Type -> Maybe DecodableType
judgeDecodableType t
 | DB.alphaBetaEquivTy t Eval.boolType = Just DecodeToBool
 | DB.alphaBetaEquivTy t Eval.natType  = Just DecodeToInt
 | otherwise = Nothing

evaluatorFor :: DecodableType -> LBExpr -> IO ()
evaluatorFor DecodeToBool = eval >=> decodeBool >=> print
evaluatorFor DecodeToInt = eval >=> decodeInt >=> print