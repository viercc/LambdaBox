module Optimization where

import Control.Category ((>>>))
import Data.Lambda.Manipulation
import Data.Lambda.Manipulation.Simplify
import LambdaBox.Untyped.Expr

newtype ShrinkOption = ShrinkOption
  { shrinkLevel :: Int
  }
  deriving (Show)

getOptimizer :: ShrinkOption -> LBExpr -> LBExpr
getOptimizer (ShrinkOption opt) =
  cond (opt >= 1) inlineLet
    >>> cond (opt >= 2) floatLetInward
    >>> cond (opt >= 2) sccAnalysis
    >>> cond (opt >= 3) heavyInlineLet
    >>>
    -- Enough inlining must happen before the first shrinkBeta
    cond (opt >= 3) shrinkBeta
    >>> cond (opt >= 1) floatLetInward
  where
    cond :: Bool -> (a -> a) -> (a -> a)
    cond p = if p then id else const id

    heavyInlineLet = customInlineLet defaultInlineConfig {inlineThreshold = 12}
