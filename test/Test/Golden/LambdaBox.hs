module Test.Golden.LambdaBox where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import System.Process

import Test.Tasty
import Test.Tasty.Golden
import System.IO (Handle, hClose)

targetPrograms, targetFunctions :: [String]
targetPrograms =
  [ "id",
    "noop",
    "abc",
    "hello",
    "echo-twice",
    "echo-twice-ow",
    "filter-even",
    "uppercase",
    "uppercase2"
  ]
targetFunctions = 
  [ "functions/add",
    "functions/apply",
    "functions/const",
    "functions/div10",
    "functions/isprime",
    "functions/mult",
    "functions/mult3",
    "functions/read-nat",
    "functions/show-nat",
    "functions/sub",
    "functions/subfast",
    "functions/succ"
  ]

inputFiles :: [String]
inputFiles =
    [ "empty.bin", "hello.bin", "abc.bin", "random.bin" ]

sourcePath :: String -> FilePath
sourcePath name = "example/" ++ name ++ ".png"

inputPath :: String -> FilePath
inputPath name = "testdata/" ++ name

goldenPath :: String -> String -> FilePath
goldenPath prefix name = "golden/lb/" ++ prefix ++ "/" ++ name ++ ".golden"

diffCmd :: FilePath -> FilePath -> [String]
diffCmd old new = [ "diff", "-u", "-a", "-b", old, new]

thereIsHandle :: Maybe Handle -> IO Handle
thereIsHandle = maybe (fail "no handle!?") return

performParse :: String -> [String] -> IO BSL.ByteString
performParse name opts = do
    let src = sourcePath name
        args = "--expr" : opts ++ [src]
        processDef = (proc "lbox" args){ std_in = CreatePipe, std_out = CreatePipe, std_err = Inherit }
    out <- withCreateProcess processDef $ \_stdin mstdout _stderr _ph -> do
        stdout <- thereIsHandle mstdout
        BS.hGetContents stdout
    return (BSL.fromStrict out)

performEvalWithInput :: FilePath -> String -> IO BSL.ByteString
performEvalWithInput inFile progName = do
    let src = sourcePath progName
        processDef = (proc "lbox" ["--shrink", "2", src]){ std_in = CreatePipe, std_out = CreatePipe, std_err = Inherit }
    inData <- BS.readFile inFile
    out <- withCreateProcess processDef $ \mstdin mstdout _stderr _ph -> do
        stdin <- thereIsHandle mstdin
        stdout <- thereIsHandle mstdout
        BS.hPut stdin inData
        hClose stdin
        BS.hGetContents stdout
    return (BSL.fromStrict out)

test_parseOnly_shrink0 :: TestTree
test_parseOnly_shrink0 = testGroup "lbox --expr --shrink 0" $ do
    name <- targetPrograms ++ targetFunctions
    pure $ goldenVsStringDiff name diffCmd (goldenPath "parse-shrink-0" name) (performParse name ["--shrink", "0"])

test_parseOnly_shrink2 :: TestTree
test_parseOnly_shrink2 = testGroup "lbox --expr --shrink 2" $ do
    name <- targetPrograms ++ targetFunctions
    pure $ goldenVsStringDiff name diffCmd (goldenPath "parse-shrink-2" name) (performParse name ["--shrink", "2"])

test_eval :: TestTree
test_eval = testGroup "lbox" $ do
    inFile <- inputFiles
    let prefix = "eval/" ++ inFile
    pure $ testGroup inFile $ do
        name <- targetPrograms
        pure $ goldenVsString name (goldenPath prefix name) (performEvalWithInput (inputPath inFile) name)
