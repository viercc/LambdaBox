module Test.Golden.TypedLambdaBox where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import System.Process

import Test.Tasty
import Test.Tasty.Golden
import System.IO (Handle)

targetFunctions :: [String]
targetFunctions = 
  [ "false", "true", "not", "assert-id-good",
    "nat-3", "nat-3-assert", "nat-succ", "nat-add", "fix", "find-min"
  ]

targetEvalArgs :: [[String]]
targetEvalArgs =
  [
    [ "false" ],
    [ "true" ],
    [ "not", "false" ],
    [ "not", "true" ],
    [ "nat-succ", "nat-3" ],
    [ "nat-add", "nat-3", "nat-3" ]
  ]

sourcePath :: String -> FilePath
sourcePath name = "typed-example/" ++ name ++ ".png"

goldenPath :: String -> String -> FilePath
goldenPath prefix name = "golden/tlb/" ++ prefix ++ "/" ++ name ++ ".golden"

diffCmd :: FilePath -> FilePath -> [String]
diffCmd old new = [ "diff", "-a", "-b", old, new]

thereIsHandle :: Maybe Handle -> IO Handle
thereIsHandle = maybe (fail "no handle!?") return

performTypeCheck :: String -> IO BSL.ByteString
performTypeCheck name = do
    let src = sourcePath name
        args = ["-m", "TypeCheck", "-v", "0", "--simpl", "--named", src]
        processDef = (proc "typed-lbox" args){ std_in = CreatePipe, std_out = CreatePipe, std_err = Inherit }
    out <- withCreateProcess processDef $ \_stdin mstdout _stderr _ph -> do
        stdout <- thereIsHandle mstdout
        BS.hGetContents stdout
    return (BSL.fromStrict out)

test_tc :: TestTree
test_tc = testGroup "typed-lbox -m TypeCheck" $ do
    name <- targetFunctions
    pure $ goldenVsStringDiff name diffCmd (goldenPath "tc" name) (performTypeCheck name)

performEval :: [String] -> IO BSL.ByteString
performEval names = do
    let srces = sourcePath <$> names
        args = ["-m", "Eval", "-v", "0"] ++ srces
        processDef = (proc "typed-lbox" args){ std_in = CreatePipe, std_out = CreatePipe, std_err = Inherit }
    out <- withCreateProcess processDef $ \_stdin mstdout _stderr _ph -> do
        stdout <- thereIsHandle mstdout
        BS.hGetContents stdout
    return (BSL.fromStrict out)

test_eval :: TestTree
test_eval = testGroup "typed-lbox -m Eval" $ do
    names <- targetEvalArgs
    let combinedNames = unwords names
    pure $ goldenVsStringDiff combinedNames diffCmd (goldenPath "eval" combinedNames) (performEval names)