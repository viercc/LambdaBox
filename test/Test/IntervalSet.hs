{-# LANGUAGE BlockArguments #-}
module Test.IntervalSet where

import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import qualified Data.IntSet as IS
import qualified Data.IntervalSet as Ival
import qualified Data.Set as Set

genKey :: Gen Int
genKey = Gen.int (Range.linear 0 63)

genInterval :: Gen (Int, Int)
genInterval = (\k d -> (k, k + d)) <$> genKey <*> Gen.int (Range.constant 1 5)

genIntSet :: Gen IS.IntSet
genIntSet = IS.fromDistinctAscList . Set.toAscList <$> Gen.set (Range.linear 0 30) genKey

gen :: Gen Ival.IntervalSet
gen = Ival.fromIntervals <$> Gen.list (Range.linear 0 6) genInterval 

hprop_fromIntSet :: Property
hprop_fromIntSet = property do
    ints <- forAll genIntSet
    ints === Ival.toIntSet (Ival.fromIntSet ints)

hprop_toIntervals :: Property
hprop_toIntervals = property do
    s <- forAll gen
    s === Ival.fromIntervals (Ival.toIntervals s)

hprop_member :: Property
hprop_member = property do
    s <- forAll gen
    k <- forAll genKey
    k `IS.member` Ival.toIntSet s === k `Ival.member` s

hprop_insert :: Property
hprop_insert = property do
    s <- forAll gen
    r@(x0,x1) <- forAll genInterval
    let insertedSet = IS.fromList [x0 .. x1 - 1]
    IS.union insertedSet (Ival.toIntSet s) === Ival.toIntSet (Ival.insert r s)

hprop_restrict :: Property
hprop_restrict = property do
    s <- forAll gen
    r@(x0,x1) <- forAll genInterval
    let restrictSet = IS.fromList [x0 .. x1 - 1]
    IS.intersection restrictSet (Ival.toIntSet s) === Ival.toIntSet (Ival.restrict r s)

hprop_union :: Property
hprop_union = property do
    s1 <- forAll gen
    s2 <- forAll gen
    IS.union (Ival.toIntSet s1) (Ival.toIntSet s2) === Ival.toIntSet (Ival.union s1 s2)

hprop_intersection :: Property
hprop_intersection = property do
    s1 <- forAll gen
    s2 <- forAll gen
    IS.intersection (Ival.toIntSet s1) (Ival.toIntSet s2) === Ival.toIntSet (Ival.intersection s1 s2)