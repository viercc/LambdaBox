module LambdaBox.Image
  ( shape,
    withinBound,
    cloneImage,
    fromList,
    LBImage,
    readImageRGBA8,
    decodeImageRGBA8,
  )
where

import Codec.Picture
import Codec.Picture.Types
import Control.Monad (forM_, (>=>))
import Control.Monad.ST (ST)
import qualified Data.ByteString as BS
import Text.Printf

---- Access to image shapes

-- | shape image = (height image, width image)
shape :: MutableImage s a -> (Int, Int)
shape image = (mutableImageHeight image, mutableImageWidth image)

-- | (x,y) is in w*h rectangle
withinBound :: (Num a, Num b, Ord a, Ord b) => a -> b -> a -> b -> Bool
withinBound h w y x = (0 <= y) && (y < h) && (0 <= x) && (x < w)

cloneImage :: (Pixel a) => MutableImage s a -> ST s (MutableImage s a)
cloneImage = freezeImage >=> unsafeThawImage

---- conversion between immutable vector

-- | Constructs image from 'List' by copying its elements
fromList :: (Pixel a) => Int -> Int -> [a] -> ST s (MutableImage s a)
fromList h w lst =
  let lst_cut = take (h * w) lst
   in if h * w == length lst_cut
        then do
          image <- newMutableImage w h
          let pos = [(i, j) | i <- [0 .. h - 1], j <- [0 .. w - 1]]
              pixels = zip pos lst_cut
          forM_ pixels $ \((i, j), val) -> writePixel image j i val
          return image
        else error $ printf "fromList: too short list: len=%d, h=%d, w=%d" (length lst) h w

type LBImage = Image PixelRGBA8

readImageRGBA8 :: String -> IO (Either String LBImage)
readImageRGBA8 path = do
  readResult <- readImage path
  return $ readResult >>= convertToRGBA8

decodeImageRGBA8 :: BS.ByteString -> Either String LBImage
decodeImageRGBA8 bs = decodeImage bs >>= convertToRGBA8

convertToRGBA8 :: DynamicImage -> Either String LBImage
convertToRGBA8 (ImageY8 image) = Right (promoteImage image)
convertToRGBA8 (ImageY16 _) = Left "conversion fail: Y16->RGBA8"
convertToRGBA8 (ImageYF _) = Left "conversion fail: YF->RGBA8"
convertToRGBA8 (ImageY32 _) = Left "conversion fail: Y32->RGBA8"
convertToRGBA8 (ImageYA8 image) = Right (promoteImage image)
convertToRGBA8 (ImageYA16 _) = Left "conversion fail: YA16->RGBA8"
convertToRGBA8 (ImageRGB8 image) = Right (promoteImage image)
convertToRGBA8 (ImageRGB16 _) = Left "conversion fail: RGB16->RGBA8"
convertToRGBA8 (ImageRGBF _) = Left "conversion fail: RGBF->RGBA8"
convertToRGBA8 (ImageRGBA8 image) = Right image
convertToRGBA8 (ImageRGBA16 _) = Left "conversion fail: RGBA16->RGBA8"
convertToRGBA8 (ImageYCbCr8 _) = Left "conversion fail: YCbCr8->RGBA8"
convertToRGBA8 (ImageCMYK8 _) = Left "conversion fail: CMYK8->RGBA8"
convertToRGBA8 (ImageCMYK16 _) = Left "conversion fail: CMYK16->RGBA8"
