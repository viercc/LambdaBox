{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE RankNTypes #-}
module LambdaBox.Typed.Expr.DeBruijn(
  Type(..),
  toNamedTy, fromNamedTy,
  alphaEquivTy, alphaBetaEquivTy,
  ClosedType,
  toClosedType,

  Term(..),
  mapA, mapX,
  bindTy,
  toNamed, fromNamed, alphaEquiv,

  simplTy, simpl,
  ClosedTerm,
  toClosedTerm,
  
  typeCheck, typeCheckWith,

  toUntyped
) where

import Text.PrettyPrint.Formattable
import Text.PrettyPrint.Util

import Control.Monad (ap, guard)
import Data.Bifunctor
import Data.Bifoldable
import Data.Bitraversable
import Data.Foldable (for_)
import Data.Maybe (isNothing)
import Data.Monoid (Sum, getSum)
import qualified Data.Map.Strict as Map

import Bound.Class
import Bound.Scope
import Bound.Scope.Extra

import qualified LambdaBox.Typed.Expr.Named as Named
import Data.Functor.Classes (Eq1(..), Eq2(..), eq1)
import AutoLift.Functor

import qualified Data.Lambda.Core as Untyped
import qualified LambdaBox.Untyped.Expr.LamUtil as Untyped

data Type a b =
      TyVar b
    | TyArr (Type a b) (Type a b)
    | TyForall a (Scope' (Type a) b)
    | TyLet a (Type a b) (Scope' (Type a) b)
    deriving (Show, Functor, Foldable, Traversable)

instance Bifunctor Type where
  first f t = case t of
    TyVar b -> TyVar b
    TyArr t1 t2 -> TyArr (first f t1) (first f t2)
    TyForall a body -> TyForall (f a) (hoistScope (first f) body)
    TyLet a t' body -> TyLet (f a) (first f t') (hoistScope (first f) body)
  
  second = fmap

instance (Eq a, Eq b) => Eq (Type a b) where
  (==) = eq1
instance (Eq a) => Eq1 (Type a)

instance Eq2 Type where
  liftEq2 eqA eqB t t' = case (t,t') of
    (TyVar b, TyVar b') -> eqB b b'
    (TyArr t1 t2, TyArr t1' t2') -> liftEq2 eqA eqB t1 t1' && liftEq2 eqA eqB t2 t2'
    (TyForall a body, TyForall a' body') -> eqA a a' && liftEq2Scope eqA eqB body body'
    (TyLet a t1 body, TyLet a' t1' body') -> eqA a a' && liftEq2 eqA eqB t1 t1' && liftEq2Scope eqA eqB body body'
    (_,_) -> False

deriving
  via Reflected1 (Type a)
  instance Show a => Show1 (Type a)

instance Applicative (Type a) where
    pure = TyVar
    (<*>) = ap

instance Monad (Type a) where
    ty >>= k = case ty of
        TyVar b -> k b
        TyArr t1 t2 -> TyArr (t1 >>= k) (t2 >>= k)
        TyForall a body -> TyForall a (body >>>= k)
        TyLet a t body  -> TyLet a (t >>= k) (body >>>= k)

instance (Formattable a, Formattable b) => Formattable (Type a b) where
  formatPrec _ (TyVar x) = formatDoc x
  formatPrec p (TyArr t u) = parensIf (p > 2) $ formatPrec 3 t <+> "->" <+> formatPrec 2 u
  formatPrec p (TyForall x body) = parensIf (p > 1) $ "∀" <> formatDoc x <> "," <+> nest 2 (formatPrec 1 body)
  formatPrec p (TyLet x t body) = parensIf (p > 1) $
    "type" <+> "{" <> line
    <> indent 2 (formatDoc x <+> "=" <+> formatPrec 0 t) <> line
    <> "}" <+> "in" <+> formatPrec 0 body

toNamedTy :: Type a a -> Named.Type a
toNamedTy = toNamedTy' Named.TyVar

toNamedTy' :: (b -> Named.Type a) -> Type a b -> Named.Type a
toNamedTy' f ty = case ty of
    TyVar a -> f a
    TyArr t1 t2 -> Named.TyArr (toNamedTy' f t1) (toNamedTy' f t2)
    TyForall a body -> Named.TyForall a (toNamedTyS a f body)
    TyLet a t body -> Named.TyLet a (toNamedTy' f t) (toNamedTyS a f body)

toNamedTyS :: a -> (b -> Named.Type a) -> Scope' (Type a) b -> Named.Type a
toNamedTyS a = scopeAlg' (Named.TyVar a) toNamedTy'

fromNamedTy :: Eq a => Named.Type a -> Type a a
fromNamedTy ty = case ty of
    Named.TyVar a -> TyVar a
    Named.TyArr t1 t2 -> TyArr (fromNamedTy t1) (fromNamedTy t2)
    Named.TyForall a body -> TyForall a (abstract1 a (fromNamedTy body))
    Named.TyLet a t body -> TyLet a (fromNamedTy t) (abstract1 a (fromNamedTy body))

alphaEquivTy :: Eq b => Type a b -> Type a' b -> Bool
alphaEquivTy = liftEq2 (\_ _ -> True) (==)

alphaBetaEquivTy :: Eq b => Type a b -> Type a' b -> Bool
alphaBetaEquivTy t u = case (t,u) of
  (TyVar b, TyVar b') -> b == b'
  (TyArr t1 t2, TyArr u1 u2) -> alphaBetaEquivTy t1 u1 && alphaBetaEquivTy t2 u2
  (TyForall _ t', TyForall _ u') -> alphaBetaEquivTy (fromScope t') (fromScope u')
  (TyLet _ t0 t', _) -> alphaBetaEquivTy (instantiate1 t0 t') u
  (_, TyLet _ u0 u') -> alphaBetaEquivTy t (instantiate1 u0 u')
  _ -> False

expandTy :: (b -> Maybe (Type a b)) -> Type a b -> Type a b
expandTy f ty = ty >>= \b -> case f b of
  Nothing -> TyVar b
  Just ty' -> expandTy f ty'

weakNormalFormWith :: (b -> Maybe (Type a b)) -> Type a b -> Type a b
weakNormalFormWith f ty = case ty of
  TyVar b -> case f b of
    Nothing -> TyVar b
    Just ty' -> weakNormalFormWith f ty'
  TyLet _ t0 t' -> weakNormalFormWith f (instantiate1 t0 t')
  _ -> ty

type ClosedType a = forall r. Type a r

toClosedType :: Type a b -> forall b'. Either b (Type a b')
toClosedType = traverse Left

data Term a x b y =
    Var y
  | Assert (Term a x b y) (Type a b)
  | App (Term a x b y) (Term a x b y)
  | AppTy (Term a x b y) (Type a b)
  | Lam x (Type a b) (ScopeVar a x b y)
  | LamTy a (ScopeTy a x b y)
  | Let x (Term a x b y) (ScopeVar a x b y)
  | LetTy a (Type a b) (ScopeTy a x b y)
  | LetRec [(x, ScopeVars a x b y)] (ScopeVars a x b y)
  deriving (Functor, Foldable, Traversable)

type ScopeVar a x b y = Scope' (Term a x b) y
type ScopeVars a x b y = Scope Int (Term a x b) y
type ScopeTy a x b y = Term a x (Var () b) y

instance (Eq a, Eq x, Eq b, Eq y) => Eq (Term a x b y) where
  (==) = eq1

instance (Eq a, Eq x, Eq b) => Eq1 (Term a x b)

instance (Eq a, Eq x) => Eq2 (Term a x) where
  liftEq2 eqB eqY e e' = case (e, e') of
    (Var y, Var y') -> eqY y y'
    (Assert e1 t, Assert e1' t') -> liftEq2 eqB eqY e1 e1' && liftEq eqB t t' 
    (App e1 e2, App e1' e2') -> liftEq2 eqB eqY e1 e1' && liftEq2 eqB eqY e2 e2'
    (AppTy e1 t, AppTy e1' t') -> liftEq2 eqB eqY e1 e1' && liftEq eqB t t'
    (Lam x t body, Lam x' t' body') -> x == x' && liftEq eqB t t' && liftEq2Scope eqB eqY body body'
    (LamTy a body, LamTy a' body') -> a == a' && liftEq2 (liftEq eqB) eqY body body'
    (Let x e1 body, Let x' e1' body') -> x == x' && liftEq2 eqB eqY e1 e1' && liftEq2Scope eqB eqY body body'
    (LetTy a t body, LetTy a' t' body') -> a == a' && liftEq eqB t t' && liftEq2 (liftEq eqB) eqY body body'
    (LetRec defs body, LetRec defs' body') -> liftEq (liftEq (liftEq2Scope eqB eqY)) defs defs' && liftEq2Scope eqB eqY body body'
    (_,_) -> False

instance Bifunctor (Term a x) where
  first f expr = case expr of
    Var y -> Var y
    Assert e t -> Assert (first f e) (fmap f t)
    App e1 e2 -> App (first f e1) (first f e2)
    AppTy e1 t -> AppTy (first f e1) (fmap f t)
    Lam x t body -> Lam x (fmap f t) (firstScope f body)
    LamTy a body -> LamTy a (first (fmap f) body)
    Let x e body -> Let x (first f e) (firstScope f body)
    LetTy a t body -> LetTy a (fmap f t) (first (fmap f) body)
    LetRec defs body -> LetRec (fmap (second (firstScope f)) defs) (firstScope f body)
  second = fmap
  bimap = bimapDefault

instance Bifoldable (Term a x) where
  bifoldMap = bifoldMapDefault

instance Bitraversable (Term a x) where
  bitraverse f g expr = case expr of
    Var y -> Var <$> g y
    Assert e t -> Assert <$> bitraverse f g e <*> traverse f t
    App e1 e2 -> App <$> bitraverse f g e1 <*> bitraverse f g e2
    AppTy e1 t -> AppTy <$> bitraverse f g e1 <*> traverse f t
    Lam x t body -> Lam x <$> traverse f t <*> bitraverseScope f g body
    LamTy a body -> LamTy a <$> bitraverse (traverse f) g body
    Let x e body -> Let x <$> bitraverse f g e <*> bitraverseScope f g body
    LetTy a t body -> LetTy a <$> traverse f t <*> bitraverse (traverse f) g body
    LetRec defs body -> LetRec <$> traverse (traverse (bitraverseScope f g)) defs <*> bitraverseScope f g body

instance Applicative (Term a x b) where
    pure = Var
    (<*>) = ap

instance Monad (Term a x b) where
    expr >>= k = case expr of
        Var y -> k y
        Assert e t -> Assert (e >>= k) t
        App e1 e2 -> App (e1 >>= k) (e2 >>= k)
        AppTy e1 t -> AppTy (e1 >>= k) t
        Lam x t body -> Lam x t (body >>>= k)
        LamTy a body -> LamTy a (body >>= (first F . k))
        Let x e body -> Let x (e >>= k) (body >>>= k)
        LetTy a t body -> LetTy a t (body >>= (first F . k))
        LetRec defs body -> LetRec (fmap (second (>>>= k)) defs) (body >>>= k)

instance (Formattable a, Formattable x, Formattable b, Formattable y) => Formattable (Term a x b y) where
  formatPrec _ (Var x) = formatDoc x
  formatPrec p (Assert e t) = parensIf (p > 0) $ formatPrec 0 e <+> ":" <+> formatPrec 1 t
  formatPrec p (App e1 e2) = parensIf (p > 10) $ formatPrec 10 e1 <+> formatPrec 11 e2
  formatPrec p (AppTy e t) = parensIf (p > 10) $ formatPrec 10 e <+> ("@" <> formatPrec 11 t)
  formatPrec p (Lam x t body) = parensIf (p > 1) $
    "\\(" <> formatDoc x <+> ":" <+> formatDoc t <>")" <+> "->"
      <+> nest 2 (formatPrec 1 body)
  formatPrec p (LamTy a body) = parensIf (p > 1) $
    "/\\" <> formatDoc a <+> "->"
      <+> nest 2 (formatPrec 1 body)
  formatPrec p (Let x e body) = parensIf (p > 1) $
    "let" <+> "{" <> line
    <+> indent 2 (formatDoc x <+> "=" <+> formatPrec 0 e) <> line
    <> "}" <+> "in" <+> formatPrec 1 body
  formatPrec p (LetTy x t body) = parensIf (p > 1) $
    "type" <+> "{" <> line
    <> indent 2 (formatDoc x <+> "=" <+> formatPrec 0 t) <> line
    <> "}" <+> "in" <+> formatPrec 1 body
  formatPrec p (LetRec defs body) = parensIf (p > 1) $
    "letrec" <+> "{" <> line
    <> indent 2 (vsep $ (\(x,e) -> formatDoc x <+> "=" <+> formatPrec 0 e <> ";") <$> defs) <> line
    <> "}" <+> "in" <+> formatPrec 1 body

toNamed :: Term a x a x -> Named.Term a x
toNamed = toNamed' Named.TyVar Named.Var

toNamed' :: (b -> Named.Type a) -> (y -> Named.Term a x) -> Term a x b y -> Named.Term a x
toNamed' f g expr = case expr of
    Var y -> g y
    Assert e t -> Named.Assert (toNamed' f g e) (toNamedTy' f t)
    App e1 e2 -> Named.App (toNamed' f g e1) (toNamed' f g e2)
    AppTy e1 t -> Named.AppTy (toNamed' f g e1) (toNamedTy' f t)
    Lam x t body -> Named.Lam x (toNamedTy' f t) (toNamedS (const x) f g body)
    LamTy a body -> Named.LamTy a (toNamed' (unvar' (Named.TyVar a) f) g body)
    Let x e body -> Named.Let x (toNamed' f g e) (toNamedS (const x) f g body)
    LetTy a t body -> Named.LetTy a (toNamedTy' f t) (toNamed' (unvar' (Named.TyVar a) f) g body)
    LetRec defs body -> Named.LetRec (fmap (second go) defs) (go body)
      where
        defVars = fst <$> defs
        go = toNamedS (defVars !!) f g

toNamedS :: (c -> x) -> (b -> Named.Type a) -> (y -> Named.Term a x) -> Scope c (Term a x b) y -> Named.Term a x
toNamedS k f = scopeAlg (Named.Var . k) (toNamed' f)

fromNamed :: (Eq a, Ord x) => Named.Term a x -> Term a x a x
fromNamed expr = case expr of
    Named.Var x -> Var x
    Named.Assert e t -> Assert (fromNamed e) (fromNamedTy t)
    Named.App e1 e2 -> App (fromNamed e1) (fromNamed e2)
    Named.AppTy e1 t -> AppTy (fromNamed e1) (fromNamedTy t)
    Named.Lam x t body -> Lam x (fromNamedTy t) (abstract1 x $ fromNamed body)
    Named.LamTy a body -> LamTy a (first (maskEq a) (fromNamed body))
    Named.Let x e body -> Let x (fromNamed e) (abstract1 x $ fromNamed body)
    Named.LetTy a t body -> LetTy a (fromNamedTy t) (first (maskEq a) (fromNamed body))
    Named.LetRec defs body -> LetRec (fmap (second (abst . fromNamed)) defs) (abst (fromNamed body))
      where
        abst = abstractMany (fst <$> defs)

mapA :: (a -> a') -> Term a x b y -> Term a' x b y
mapA f expr = case expr of
  Var y -> Var y
  Assert e t -> Assert (mapA f e) (first f t)
  App e1 e2 -> App (mapA f e1) (mapA f e2)
  AppTy e1 t -> AppTy (mapA f e1) (first f t)
  Lam x t body -> Lam x (first f t) (hoistScope (mapA f) body)
  LamTy a body -> LamTy (f a) (mapA f body)
  Let x e body -> Let x (mapA f e) (hoistScope (mapA f) body)
  LetTy a t body -> LetTy (f a) (first f t) (mapA f body)
  LetRec defs body -> LetRec (fmap (second (hoistScope (mapA f))) defs) (hoistScope (mapA f) body)

mapX :: (x -> x') -> Term a x b y -> Term a x' b y
mapX f expr = case expr of
  Var y -> Var y
  Assert e t -> Assert (mapX f e) t
  App e1 e2 -> App (mapX f e1) (mapX f e2)
  AppTy e1 t -> AppTy (mapX f e1) t
  Lam x t body -> Lam (f x) t (hoistScope (mapX f) body)
  LamTy a body -> LamTy a (mapX f body)
  Let x e body -> Let (f x) (mapX f e) (hoistScope (mapX f) body)
  LetTy a t body -> LetTy a t (mapX f body)
  LetRec defs body -> LetRec (fmap (bimap f (hoistScope (mapX f))) defs) (hoistScope (mapX f) body)

bindTy :: (b -> Type a b') -> Term a x b y -> Term a x b' y
bindTy f expr = case expr of
  Var y -> Var y
  Assert e t -> Assert (bindTy f e) (t >>= f)
  App e1 e2 -> App (bindTy f e1) (bindTy f e2)
  AppTy e1 t -> AppTy (bindTy f e1) (t >>= f)
  Lam x t body -> Lam x (t >>= f) (bindS body)
  LamTy a body -> LamTy a (bindTy (traverse f) body)
  Let x e body -> Let x (bindTy f e) (bindS body)
  LetTy a t body -> LetTy a (t >>= f) (bindTy (traverse f) body)
  LetRec defs body -> LetRec (fmap (second bindS) defs) (bindS body)
  where
    bindS = hoistScope (bindTy f)

alphaEquiv :: (Eq b, Eq y) => Term a x b y -> Term a' x' b y -> Bool
alphaEquiv e f = case (e,f) of
  (Var y, Var y') -> y == y'
  (Assert e1 t, Assert e1' t') -> alphaEquiv e1 e1' && alphaEquivTy t t'
  (App e1 e2, App f1 f2) -> alphaEquiv e1 f1 && alphaEquiv e2 f2
  (AppTy e1 t, AppTy f1 u) -> alphaEquiv e1 f1 && alphaEquivTy t u
  (Lam _ t e', Lam _ u f') -> alphaEquivTy t u && alphaEquiv (fromScope e') (fromScope f')
  (LamTy _ e', LamTy _ f') -> alphaEquiv e' f'
  (Let _ e0 e', Let _ f0 f') -> alphaEquiv e0 f0 && alphaEquiv (fromScope e') (fromScope f')
  (LetTy _ t e', LetTy _ u f') -> alphaEquivTy t u && alphaEquiv e' f'
  (LetRec defs body, LetRec defs' body') ->
    all (\(r,r') -> alphaEquiv (fromScope r) (fromScope r')) (zip (snd <$> defs) (snd <$> defs')) &&
      alphaEquiv (fromScope body) (fromScope body')
  _ -> False

simplTy :: Type a b -> Type a b
simplTy = simplTy' TyVar

simplTy' :: (b -> Type a c) -> Type a b -> Type a c
simplTy' subst ty = case ty of
  TyVar b -> subst b
  TyArr t1 t2 -> TyArr (simplTy' subst t1) (simplTy' subst t2)
  TyForall a body -> TyForall a (simpleTyS subst body)
  TyLet a t body ->
    let t' = simplTy' subst t
     in case t' of
          TyVar _ -> scopeAlg' t' simplTy' subst body
          _ | countBound body <= 1 -> scopeAlg' t' simplTy' subst body
            | otherwise            -> TyLet a t' (simpleTyS subst body)

simpleTyS :: (b -> Type a c) -> Scope' (Type a) b -> Scope' (Type a) c
simpleTyS = scopeBinder simplTy'

simpl :: Term a x b y -> Term a x b y
simpl = simpl' TyVar Var

simpl' :: (b -> Type a b') -> (y -> Term a x b' y') -> Term a x b y -> Term a x b' y'
simpl' substTy subst expr = case expr of
  Var y -> subst y
  Assert e1 t -> Assert (simpl' substTy subst e1) (simplTy' substTy t)
  App e1 e2 -> App (simpl' substTy subst e1) (simpl' substTy subst e2)
  AppTy e1 t -> AppTy (simpl' substTy subst e1) (simplTy' substTy t)
  Lam x t body -> Lam x (simplTy' substTy t) (simplS substTy subst body)
  LamTy a body -> LamTy a (simplSty substTy subst body)
  Let x e body ->
    let e' = simpl' substTy subst e
    in case e' of
          Var _ -> scopeAlg' e' (simpl' substTy) subst body
          _ | countBound body <= 1 -> scopeAlg' e' (simpl' substTy) subst body
            | otherwise            -> Let x e' (simplS substTy subst body)
  LetTy a t body ->
    let t' = simplTy' substTy t
        substTy' = unvar' t' substTy
     in case t' of
          TyVar _ -> simpl' substTy' subst body
          _ | countBoundTy body <= 1 -> simpl' substTy' subst body
            | otherwise              -> LetTy a t' (simplSty substTy subst body)
  LetRec defs body ->
    let goDown = simplS substTy subst
        defsMap0 = Map.fromList $ zip [0..] (fmap (second (fromScope . goDown)) defs)
        body0 = fromScope (goDown body)
    in simplLetRec defsMap0 body0

simplLetRec :: Ord k => Map.Map k (x, Term a x b (Var k y)) -> Term a x b (Var k y) -> Term a x b y
simplLetRec defsMap0 body0 = loop defsMap0 body0 (Map.keys defsMap0)
  where
    loop defsMap body' [] = makeLetRec (Map.toList defsMap) body'
    loop defsMap body' (k:ks) =
      let usage = countBoundUsage k body' <> foldMap (countBoundUsage k . snd) defsMap
          mRhs = Map.lookup k defsMap
          isVar (Var _) = True
          isVar _ = False
          occursCheck = foldMap (countBoundUsage k . snd) mRhs <= 0
      in case mRhs of
            Just (_, rhs) | usage == 0 || (occursCheck && (usage == 1 || isVar rhs)) ->
              let substK = unvar (\j -> if j == k then rhs else pure (B j)) (pure . F)
                  defsMap' = Map.delete k defsMap
                  defsMap'' = Map.map (second (>>= substK)) defsMap'
                  body'' = body' >>= substK
              in loop defsMap'' body'' ks
            _ -> loop defsMap body' ks

countBoundUsage :: (Eq c, Foldable f) => c -> f (Var c a) -> Sum Int
countBoundUsage c = foldMap (unvar (\c' -> if c == c' then 1 else 0) mempty)

countBoundTy :: ScopeTy a x b y -> Int
countBoundTy = getSum . bifoldMap (unvar' 1 (const 0)) (const 0)

-- | Remove scope if no variable bound in the scope is used
unscopeUnusedBound :: Type a (Var c b) -> Maybe (Type a b)
unscopeUnusedBound = traverse (unvar (const Nothing) Just)

-- simpl under "type scope"
simplSty :: (b -> Type a b') -> (y -> Term a x b' y') -> ScopeTy a x b y -> ScopeTy a x b' y'
simplSty substTy subst = simpl' (traverse substTy) (first F . subst)

-- simpl under scope
simplS :: (b -> Type a b') -> (y -> Term a x b' y') -> Scope c (Term a x b) y -> Scope c (Term a x b') y'
simplS substTy = scopeBinder (simpl' substTy)

makeLetRec :: Ord k => [(k, (x, Term a x b (Var k y)))] -> Term a x b (Var k y) -> Term a x b y
makeLetRec defs body =
  let table = Map.fromList [ (k,i) | ((k,_), i) <- zip defs [0..] ]
      rescope = abstractEither (unvar (Left . (table Map.!)) Right)
      defs' = fmap (second rescope . snd) defs
      body' = rescope body
  in LetRec defs' body'

type ClosedTerm a x = forall b y. Term a x b y

toClosedTerm :: Term a x b y -> forall b' y'. Either (Either b y) (Term a x b' y')
toClosedTerm = bitraverse (Left . Left) (Left . Right)

---------------------------------------------------

-- * Type Checking

typeCheck :: Eq b => Term a x b y -> Maybe (Type a b)
typeCheck = typeCheckWith (const Nothing)

typeCheckWith :: Eq b => (y -> Maybe (Type a b)) -> Term a x b y -> Maybe (Type a b)
typeCheckWith = typeCheckWith' (const Nothing)

typeCheckWith' :: Eq b => (b -> Maybe (Type a b)) -> (y -> Maybe (Type a b)) -> Term a x b y -> Maybe (Type a b)
typeCheckWith' envTy env expr = case expr of
  Var y -> env y
  Assert e1 t -> do
    t1 <- typeCheckWith' envTy env e1
    guard $ alphaBetaEquivTy (expandTy envTy t1) (expandTy envTy t)
    pure t
  App e1 e2 -> do
    t1 <- typeCheckWith' envTy env e1
    TyArr uExpect v <- Just $ weakNormalFormWith envTy t1
    u <- typeCheckWith' envTy env e2
    guard $ alphaBetaEquivTy (expandTy envTy uExpect) (expandTy envTy u)
    pure v
  AppTy e1 t -> do
    t1 <- typeCheckWith' envTy env e1
    TyForall _ t1' <- Just $ weakNormalFormWith envTy t1
    pure $ instantiate1 t t1'
  Lam _ t body -> do
    v <- scopeAlg' (Just t) (typeCheckWith' envTy) env body
    pure $ TyArr t v
  LamTy a body -> do
    let env' = fmap (fmap F) . env
        envTy' = unvar' Nothing (fmap (fmap F) . envTy)
    t' <- typeCheckWith' envTy' env' body
    pure $ TyForall a (toScope t')
  Let _ e body -> do
    u <- typeCheckWith' envTy env e
    scopeAlg' (Just u) (typeCheckWith' envTy) env body
  LetTy a t body -> do
    let env' = fmap (fmap F) . env
        envTy' = unvar' (Just (fmap F t)) (fmap (fmap F) . envTy)
    tbody <- typeCheckWith' envTy' env' body
    pure $ case unscopeUnusedBound tbody of
      Nothing     -> TyLet a t (toScope tbody)
      Just tbody' -> tbody'
  LetRec defs body -> do
    -- Guess the recursive definitions.
    -- If any one definition can't be guessed its type, 
    -- the type checking fails.
    tys <- sequenceA (guessLetRec envTy env defs)
    let typeCheckS = scopeAlg (Just . (tys !!)) (typeCheckWith' envTy) env
    -- Confirm that the guessed type actually checks OK.
    for_ (zip defs tys) $ \((_,e), tExpect) -> do
      t <- typeCheckS e
      guard $ alphaBetaEquivTy (expandTy envTy tExpect) (expandTy envTy t)
    -- Type check the body using the confirmed types of recursive definitions
    typeCheckS body

-- | 'typeCheckWith'' but optimistic. i.e. it returns \"if it would type-checks, its type will be ...\"
typeGuessWith' :: (b -> Maybe (Type a b)) -> (y -> Maybe (Type a b)) -> Term a x b y -> Maybe (Type a b)
typeGuessWith' envTy env expr = case expr of
  Var y -> env y
  Assert _e1 t -> pure t -- Do not actually checks (_e1 : t)
  App e1 _e2 -> do
    t1 <- typeGuessWith' envTy env e1
    TyArr _uExpect v <- Just $ weakNormalFormWith envTy t1
    -- Do not actually checks (_e2 : _u) && (_u ~ _uExpect)
    pure v
  AppTy e1 t -> do
    t1 <- typeGuessWith' envTy env e1
    TyForall _ t1' <- Just $ weakNormalFormWith envTy t1
    pure $ instantiate1 t t1'
  Lam _ t body -> do
    v <- scopeAlg' (Just t) (typeGuessWith' envTy) env body
    pure $ TyArr t v
  LamTy a body -> do
    let env' = fmap (fmap F) . env
        envTy' = unvar' Nothing (fmap (fmap F) . envTy)
    t' <- typeGuessWith' envTy' env' body
    pure $ TyForall a (toScope t')
  Let _ e body -> do
    u <- typeGuessWith' envTy env e
    scopeAlg' (Just u) (typeGuessWith' envTy) env body
  LetTy a t body -> do
    let env' = fmap (fmap F) . env
        envTy' = unvar' (Just (fmap F t)) (fmap (fmap F) . envTy)
    tbody <- typeGuessWith' envTy' env' body
    pure $ case unscopeUnusedBound tbody of
      Nothing     -> TyLet a t (toScope tbody)
      Just tbody' -> tbody'
  LetRec defs body ->
    let tys = guessLetRec envTy env defs
    in scopeAlg (tys !!) (typeGuessWith' envTy) env body

guessLetRec :: (b -> Maybe (Type a b)) -> (y -> Maybe (Type a b)) -> [(x, ScopeVars a x b y)] -> [Maybe (Type a b)]
guessLetRec envTy env defs = loop (Nothing <$ defs')
  where
    defs' = fromScope . snd <$> defs
    loop tys =
      let tys' = update tys <$> zip defs' tys
      in if and [ isNothing old == isNothing new | (old, new) <- zip tys tys' ]
            then tys
            else loop tys'
    update tys (e, Nothing) = typeGuessWith' envTy (unvar (tys !!) env) e
    update _ (_, Just ty) = Just ty

-- * Lower to untyped expr

toUntyped :: Term a x b y -> Untyped.Lam x l litTy y
toUntyped expr = case expr of
  Var y -> Untyped.fv_ y
  Assert e _ -> toUntyped e
  App e1 e2 -> Untyped.app_ (toUntyped e1) (toUntyped e2)
  AppTy e _ -> toUntyped e
  Lam x _ body -> Untyped.abs_ x (toUntypedScope (const 0) body)
  LamTy _ body -> toUntyped body
  Let x rhs body -> Untyped.let_ [(x, toUntyped rhs)] (toUntypedScope (const 0) body)
  LetTy _ _ body -> toUntyped body
  LetRec defs body -> Untyped.letrec_ (map (second (toUntypedScope id)) defs) (toUntypedScope id body)

toUntypedScope :: (c -> Int) -> Scope c (Term a x b) y -> Untyped.Scope (Untyped.Lam x l litTy y)
toUntypedScope bvIdx body
  = Untyped.abstractEither (unvar (Left . bvIdx) Right) $ toUntyped $ fromScope body
