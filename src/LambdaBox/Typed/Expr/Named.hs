{-# LANGUAGE OverloadedStrings #-}
module LambdaBox.Typed.Expr.Named where

import Text.PrettyPrint.Formattable
import Text.PrettyPrint.Util

import qualified Data.Map as Map
import Data.Foldable (Foldable(..))
import Data.Bifunctor (Bifunctor(..))

data Type a =
      TyVar a
    | TyArr (Type a) (Type a)
    | TyForall a (Type a)
    | TyLet a (Type a) (Type a)
    deriving (Eq, Show)

instance Formattable a => Formattable (Type a) where
  formatPrec _ (TyVar x) = formatDoc x
  formatPrec p (TyArr t u) = parensIf (p > 2) $ formatPrec 3 t <+> "->" <+> formatPrec 2 u
  formatPrec p (TyForall x body) = parensIf (p > 1) $ "∀" <> formatDoc x <> "," <+> nest 2 (formatPrec 1 body)
  formatPrec p (TyLet x t body) = parensIf (p > 1) $
    "type" <+> "{" <> line
    <> indent 2 (formatDoc x <+> "=" <+> formatPrec 0 t) <> line
    <> "}" <+> "in" <+> formatPrec 0 body


data Term a x =
    Var x
  | Assert (Term a x) (Type a)
  | App (Term a x) (Term a x)
  | AppTy (Term a x) (Type a)
  | Lam x (Type a) (Term a x)
  | LamTy a (Term a x)
  | Let x (Term a x) (Term a x)
  | LetTy a (Type a) (Term a x)
  | LetRec [(x, Term a x)] (Term a x)
  deriving (Eq, Show)

instance (Formattable a, Formattable x) => Formattable (Term a x) where
  formatPrec _ (Var x) = formatDoc x
  formatPrec p (Assert e t) = parensIf (p > 0) $ formatPrec 0 e <+> ":" <+> formatPrec 1 t 
  formatPrec p (App e1 e2) = parensIf (p > 10) $ formatPrec 10 e1 <+> formatPrec 11 e2
  formatPrec p (AppTy e t) = parensIf (p > 10) $ formatPrec 10 e <+> ("@" <> formatPrec 11 t)
  formatPrec p (Lam x t body) = parensIf (p > 1) $
    "\\(" <> formatDoc x <+> ":" <+> formatDoc t <>")" <+> "->"
      <+> nest 2 (formatPrec 1 body)
  formatPrec p (LamTy a body) = parensIf (p > 1) $
    "/\\" <> formatDoc a <+> "->"
      <+> nest 2 (formatPrec 1 body)
  formatPrec p (Let x e body) = parensIf (p > 1) $
    "let" <+> "{" <> line
    <+> indent 2 (formatDoc x <+> "=" <+> formatPrec 0 e) <> line
    <> "}" <+> "in" <+> formatPrec 1 body
  formatPrec p (LetTy x t body) = parensIf (p > 1) $
    "type" <+> "{" <> line
    <> indent 2 (formatDoc x <+> "=" <+> formatPrec 0 t) <> line
    <> "}" <+> "in" <+> formatPrec 1 body
  formatPrec p (LetRec defs body) = parensIf (p > 1) $
    "letrec" <+> "{" <> line
    <+> indent 2 (vsep $ (\(x,e) -> formatDoc x <+> "=" <+> formatPrec 0 e <> ";") <$> defs) <> line
    <> "}" <+> "in" <+> formatPrec 1 body


simplTy :: Ord a => Type a -> Type a
simplTy = elimSingleTy Map.empty

simpl :: (Ord a, Ord x) => Term a x -> Term a x
simpl = elimSingle Map.empty Map.empty

tyVarUsageTy :: Eq a => a -> Type a -> Int
tyVarUsageTy b = go
  where
    go ty = case ty of
      TyVar a -> if a == b then 1 else 0
      TyArr t1 t2 -> go t1 + go t2
      TyForall a body -> if a == b then 0 else go body
      TyLet a t body -> go t + if a == b then 0 else go body

varUsage :: Eq x => x -> Term a x -> Int
varUsage y = go
  where
    go expr = case expr of
      Var x -> if x == y then 1 else 0
      Assert e _ -> go e
      App e1 e2 -> go e1 + go e2
      AppTy e _ -> go e
      Lam x _ body -> if x == y then 0 else go body
      LamTy _ body -> go body
      Let x e body -> go e + if x == y then 0 else go body
      LetTy _ _ body -> go body
      LetRec defs body -> if y `elem` map fst defs then 0 else sum (go . snd <$> defs) + go body

tyVarUsage :: Eq a => a -> Term a x -> Int
tyVarUsage b = go
  where
    goTy = tyVarUsageTy b
    go expr = case expr of
      Var _ -> 0
      Assert _ t -> goTy t
      App e1 e2 -> go e1 + go e2
      AppTy _ t -> goTy t
      Lam _ t body -> goTy t + go body
      LamTy a body -> if a == b then 0 else go body
      Let _ e body -> go e + go body
      LetTy a t body -> goTy t + if a == b then 0 else go body
      LetRec defs body -> sum (go . snd <$> defs) + go body

-- | Eliminate @TyLet a t body@ by substitution, only if
--
--   * @t = TyVar b@
--   * @a@ is used at most once in @body@
elimSingleTy :: Ord a => Map.Map a (Type a) -> Type a -> Type a
elimSingleTy subst ty = case ty of
  TyVar a -> applySubstTy subst a
  TyArr t1 t2 -> TyArr (elimSingleTy subst t1) (elimSingleTy subst t2)
  TyForall a body -> TyForall a (elimSingleTy (Map.delete a subst) body)
  TyLet a t body ->
    let t' = elimSingleTy subst t
        substUseA = Map.insert a t' subst
        substMaskA = Map.delete a subst
    in case t' of
          TyVar _ -> elimSingleTy substUseA body
          _       -> case tyVarUsageTy a body of
            0 -> elimSingleTy substMaskA body
            1 -> elimSingleTy substUseA body
            _ -> TyLet a t' (elimSingleTy substMaskA body)

applySubstTy :: Ord a => Map.Map a (Type a) -> a -> Type a
applySubstTy subst a = Map.findWithDefault (TyVar a) a subst

-- | Eliminate @Let x e body@ and @LetTy a t body@ by substitutions
elimSingle :: (Ord a, Ord x) => Map.Map a (Type a) -> Map.Map x (Term a x) -> Term a x -> Term a x
elimSingle substTy subst expr = case expr of
  Var x -> applySubst subst x
  Assert e t -> Assert (elimSingle substTy subst e) (elimSingleTy substTy t)
  App e1 e2 -> App (elimSingle substTy subst e1) (elimSingle substTy subst e2)
  AppTy e t -> AppTy (elimSingle substTy subst e) (elimSingleTy substTy t)
  Lam x t body -> Lam x (elimSingleTy substTy t) (elimSingle substTy (Map.delete x subst) body)
  LamTy a body -> LamTy a (elimSingle (Map.delete a substTy) subst body)
  Let x e body ->
    let e' = elimSingle substTy subst e
        substUseX = Map.insert x e' subst
        substMaskX = Map.delete x subst
    in case e' of
          Var _ -> elimSingle substTy substUseX body
          _     -> case varUsage x body of
            0 -> elimSingle substTy substMaskX body
            1 -> elimSingle substTy substUseX body
            _ -> Let x e' (elimSingle substTy substMaskX body)
  LetTy a t body ->
    let t' = elimSingleTy substTy t
        substUseA = Map.insert a t' substTy
        substMaskA = Map.delete a substTy
    in case t' of
          TyVar _ -> elimSingle substUseA subst body
          _       -> case tyVarUsage a body of
            0 -> elimSingle substMaskA subst body
            1 -> elimSingle substUseA subst body
            _ -> LetTy a t' (elimSingle substMaskA subst body)
  LetRec defs body ->
    let substMaskXs = foldl' (flip Map.delete) subst (fst <$> defs)
    in LetRec (second (elimSingle substTy substMaskXs) <$> defs) (elimSingle substTy substMaskXs body)

applySubst :: Ord x => Map.Map x (Term a x) -> x -> Term a x
applySubst subst x = Map.findWithDefault (Var x) x subst
