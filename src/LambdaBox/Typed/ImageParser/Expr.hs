{-# LANGUAGE TupleSections #-}

module LambdaBox.Typed.ImageParser.Expr
  (
    VarName(..), Type', Expr', buildExpr,
    -- * Debugging
    buildFromPreExpr,
    PreExpr(..),
  )
where

import Prelude hiding (lines)
import Data.Traversable (forM)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map.Strict (Map, (!))
import qualified Data.Map.Strict as Map
import Data.Graph (stronglyConnCompR, SCC (..))

import Data.Either.Validation

import Text.PrettyPrint.Formattable

import LambdaBox.Error
import LambdaBox.Diagram
import LambdaBox.Typed.ImageParser.PreExpr
import LambdaBox.Typed.Expr.Named

type Type' = Type VarName
type Expr' = Term VarName VarName

-- * Error utilities

report :: [e] -> Validation [e] ()
report es
  | null es = pure ()
  | otherwise = Failure es

posTop :: LBPosition
posTop = LBPosStr "Toplevel"

------------------------------------------------------------
--  Circuit to Expr
------------------------------------------------------------
buildExpr :: Diagram -> Either [LBError] Expr'
buildExpr diag = do
  preExpr <- mkPreExpr diag
  buildFromPreExpr diag (simplPreExpr preExpr)

buildFromPreExpr :: Diagram -> PreExpr VarName -> Either [LBError] Expr'
buildFromPreExpr diag preExpr = do
  validationToEither $ checkUndefinedVars diag preExpr
  formExpr diag preExpr

describeVar :: Diagram -> VarName -> String
describeVar _ (A bid) = "Arg of box " ++ format bid
describeVar _ (B bid) = "Box " ++ format bid
describeVar d (W wid) = "Wire " ++ format wid ++ ", " ++ format (_lines (_diagramWires d ! wid))

updatePos :: Diagram -> VarName -> LBPosition -> LBPosition
updatePos diag x pos = case x of
    A _ -> pos
    B bid -> LBPosBox "" (_bb (_diagramBoxes diag ! bid))
    W _ -> pos

checkUndefinedVars :: Diagram -> PreExpr VarName -> Validation [LBError] ()
checkUndefinedVars diag exprTop = report $ go posTop Set.empty exprTop
  where
    err pos x = LBError pos $ "Undefined variable: " ++ describeVar diag x

    go :: LBPosition -> Set VarName -> PreExpr VarName -> [LBError]
    go pos env expr = case expr of
      PreVar x -> [ err pos x | x `Set.notMember` env ]
      PreLet defs ret ->
        let env' = Set.union env (Set.fromList (fst <$> defs))
            defErrors = defs >>= \(x, rhs) -> go (updatePos diag x pos) env' rhs
         in [ err pos ret | ret `Set.notMember` env' ] ++ defErrors
      PreApp e1 e2 -> go pos env e1 ++ go pos env e2
      PreAbs x ty body ->
        let env' = Set.insert x env
         in go pos env ty ++ go pos env' body
      PreTyAbs x body ->
        let env' = Set.insert x env
         in go pos env' body
      PreTyArr e1 e2 -> go pos env e1 ++ go pos env e2

data Sort = TypeWire | TermWire
    deriving (Eq, Ord, Show)

type Context = Map VarName Sort

type TypeOrTerm = Either Type' Expr'

getSort :: TypeOrTerm -> Sort
getSort (Left _) = TypeWire
getSort (Right _) = TermWire

topsort :: [(VarName, PreExpr VarName)] -> [ SCC (VarName, PreExpr VarName) ]
topsort defs = fmap postproc <$> stronglyConnCompR [ (expr, x, Set.toList (freeVarsIn xs expr)) | (x, expr) <- defs ]
  where
    xs = Set.fromList (fst <$> defs)
    postproc (expr, x, _deps) = (x, expr)

freeVarsIn :: Ord x => Set x -> PreExpr x -> Set x
freeVarsIn xs expr
  | Set.null xs = Set.empty
  | otherwise = case expr of
      PreVar x
        | x `Set.member` xs -> Set.singleton x
        | otherwise -> Set.empty
      PreApp e1 e2 -> Set.union (freeVarsIn xs e1) (freeVarsIn xs e2)
      PreTyArr e1 e2 -> Set.union (freeVarsIn xs e1) (freeVarsIn xs e2)
      PreAbs x ty body -> Set.union (freeVarsIn xs ty) (freeVarsIn (Set.delete x xs) body)
      PreTyAbs x ty -> freeVarsIn (Set.delete x xs) ty
      PreLet defs ret ->
        let ys = Set.fromList (fst <$> defs)
            xs' = xs Set.\\ ys
            retSet | ret `Set.member` xs' = Set.singleton ret
                   | otherwise            = Set.empty
            rhsSet = Set.unions $ freeVarsIn xs' . snd <$> defs
        in Set.union rhsSet retSet

data LetDefs = LetDef VarName Expr' | LetTyDef VarName Type' | LetRecDefs [(VarName, Expr')]

formExpr :: Diagram -> PreExpr VarName -> Either [LBError] Expr'
formExpr diag exprTop = case go posTop Map.empty exprTop of
    Left es -> Left es
    Right (Left ty) -> Left
      [ LBError posTop $ "Final result must be a term, but it was a type: " ++ show ty ]
    Right (Right e') -> Right e'
  where
    errTypeAtTerm pos = Left $ pure $ LBError pos "Type at term position!"
    errTermAtType pos = Left $ pure $ LBError pos "Term at type position!"
    errApplyTyTy pos ty1 ty2 = Left $ pure $ LBError pos $ "Unsupported type-type application: (" ++ format ty1 ++ ") (" ++ format ty2 ++ ")"

    goType :: LBPosition -> Context -> PreExpr VarName -> Either [LBError] Type'
    goType pos env expr =
      do res <- go pos env expr
         case res of
           Left ty -> pure ty
           Right _ -> errTermAtType pos

    goTerm :: LBPosition -> Context -> PreExpr VarName -> Either [LBError] Expr'
    goTerm pos env expr =
      do res <- go pos env expr
         case res of
           Left _ -> errTypeAtTerm pos
           Right e -> pure e

    errStr pos x = " pos = " ++ format pos ++ "; x = " ++ format x ++ " at " ++ format (updatePos diag x pos)

    go :: LBPosition -> Context -> PreExpr VarName -> Either [LBError] TypeOrTerm
    go pos env expr = case expr of
      PreVar x -> case Map.lookup x env of
        Just TermWire -> pure $ Right (Var x)
        Just TypeWire -> pure $ Left (TyVar x)
        Nothing -> error $ "Unexpected (bug!)"  ++ errStr pos x
      PreLet defs ret -> loop env [] (topsort defs)
        where
          loop env' stack [] = case Map.lookup ret env' of
            Just TermWire -> pure $ Right $ rebuildTermLet stack (Var ret)
            Just TypeWire -> Left <$> rebuildTypeLet stack (TyVar ret)
            _ -> error $ "Unexpected (bug!);" ++ errStr pos ret
          loop env' stack (AcyclicSCC (x, rhs) : rest) =
            do let pos' = updatePos diag x pos
               rhs' <- go pos' env' rhs
               let env'' = Map.insert x (getSort rhs') env'
               case rhs' of
                 Left rhsTy -> loop env'' (LetTyDef x rhsTy : stack) rest
                 Right rhsE -> loop env'' (LetDef x rhsE : stack) rest
          loop env' stack (CyclicSCC recDefs : rest) =
            do let env'' = Map.union (Map.fromList [ (x, TermWire) | (x,_) <- recDefs]) env'
               recDefs' <- forM recDefs $ \(x, rhs) -> do
                 let pos' = updatePos diag x pos
                 rhs' <- goTerm pos' env'' rhs
                 pure (x, rhs')
               loop env'' (LetRecDefs recDefs' : stack) rest

          rebuildTermLet [] body = body
          rebuildTermLet (def : stack) body = case def of
            LetDef x eRhs -> rebuildTermLet stack (Let x eRhs body)
            LetTyDef x tyRhs -> rebuildTermLet stack (LetTy x tyRhs body)
            LetRecDefs defs' -> rebuildTermLet stack (LetRec defs' body)

          rebuildTypeLet [] body = pure body
          rebuildTypeLet (def : stack) body = case def of
            LetTyDef x ty -> rebuildTypeLet stack (TyLet x ty body)
            _ -> errTermAtType pos
      PreAbs x ty body -> do
        ty' <- goType pos env ty
        body' <- goTerm pos (Map.insert x TermWire env) body
        pure $ Right $ Lam x ty' body'
      PreTyAbs x body -> do
        body' <- go pos (Map.insert x TypeWire env) body
        case body' of
          Left tyBody    -> pure $ Left $ TyForall x tyBody
          Right termBody -> pure $ Right $ LamTy x termBody
      PreApp e1 e2 -> do
        r1 <- go pos env e1
        r2 <- go pos env e2
        case (r1, r2) of
          (Left ty1, Left ty2) -> errApplyTyTy pos ty1 ty2
          (Left ty1, Right e2') -> pure $ Right $ Assert e2' ty1
          (Right e1', Left ty2) -> pure $ Right $ AppTy e1' ty2
          (Right e1', Right e2') -> pure $ Right $ App e1' e2'
      PreTyArr e1 e2 -> do
        e1' <- goType pos env e1
        e2' <- goType pos env e2
        pure $ Left $ TyArr e1' e2'