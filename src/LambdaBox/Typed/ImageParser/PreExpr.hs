{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedStrings #-}
module LambdaBox.Typed.ImageParser.PreExpr
  ( VarName(..), PreExpr(..), mkPreExpr, simplPreExpr )
where

-- import Debug.Trace

import Control.Monad

import Data.Foldable (traverse_)
import Data.List (groupBy, sortBy, foldl')
import Data.Function (on)
import Data.Maybe (mapMaybe)
import qualified Data.Set as Set
import Data.Map.Strict (Map, (!))
import qualified Data.Map.Strict as Map
import qualified Data.Relation as Rel

import Data.Either.Validation

import Text.PrettyPrint.Formattable
import Prettyprinter
import Text.PrettyPrint.Util (parensIf)

import LambdaBox.Error
import LambdaBox.Diagram
import Geom.Rect
import Geom.Line
import Geom.RectUtil
import Data.Either (partitionEithers)

-- * Types


data VarName = W CCID | A CCID | B CCID
    deriving (Eq, Ord, Show)

instance Pretty VarName where
  pretty v = case v of
      A x -> "a" <> formatDoc x
      B x -> "b" <> formatDoc x
      W x -> "w" <> formatDoc x

instance Formattable VarName where
    formatPrec _ = pretty

data PreExpr x =
    PreVar x
  | PreApp (PreExpr x) (PreExpr x)
  | PreTyArr (PreExpr x) (PreExpr x)
  | PreAbs x (PreExpr x) (PreExpr x)
  | PreTyAbs x (PreExpr x)
  | PreLet [(x, PreExpr x)] x
  deriving (Eq, Show)

instance Formattable x => Formattable (PreExpr x) where
  formatPrec p e = case e of
    PreVar x -> formatPrec p x
    PreApp e1 e2 -> parensIf (p > 10) $ formatPrec 10 e1 <+> formatPrec 11 e2
    PreTyArr e1 e2 -> parensIf (p > 2) $ formatPrec 3 e1 <+> "->" <+> formatPrec 2 e2
    PreAbs x t body -> parensIf (p > 1) $ "\\(" <> formatDoc x <+> ":" <+> formatDoc t <> ")" <+> "->"
      <+> nest 2 (formatPrec 1 body)
    PreTyAbs x body -> parensIf (p > 1) $ "\\\\\\" <> formatDoc x <+> "->"
      <+> nest 2 (formatPrec 1 body)
    PreLet defs body -> parensIf (p > 1) $
      "let" <+> "{" <> line
        <> indent 2 (vsep ((\(x,rhs) -> formatDoc x <+> "=" <+> formatPrec 0 rhs <> ";") <$> defs)) <> line
        <> "}" <+> "in" <+> formatPrec 1 body


-- * Intermediates

data BoxAttr
  = AbsBox
      { _boxOut :: ![CCID],
        _boxTy :: !CCID,
        _boxVar :: ![CCID],
        _boxRet :: !CCID
      }
  | TyAbsBox
      { _boxOut :: ![CCID],
        _boxTyVar :: ![CCID],
        _boxRet :: !CCID }
  | AppBox
      { _boxOut :: ![CCID],
        _boxFun :: !CCID,
        _boxArg :: !CCID
      }
  | TyArrBox
      { _boxOut :: ![CCID],
        _boxTyA :: !CCID,
        _boxTyB :: !CCID
      }
  deriving (Show, Eq)

type Wire = [Line]

mkBox :: CCBox -> Either [LBError] (Rect, BoxAttr)
mkBox dat = (rect, ) <$> attr
  where
    rect = _bb dat
    conns = _connects dat
    pos = LBPosBox "" rect
    attr = case _boxType dat of
        Square -> mkAppBox pos conns
        FilledSquare -> mkTyArrBox pos conns
        ThinBox -> mkAbsBox pos conns
        ThickBox -> mkTyAbsBox pos conns

{-

# Abs
          | Ty
    +---+-+------+
    |   | Var    |
Out-+   |        +-Out
    |   | Ret    |
    +---+-+------+
          | Out

# App (3 x 3)
         Arg
          |
        +-+-+
    Fun-+   +-Out
        +-+-+
          |
         Out

# TyAbs

    +----+--------+
    +----+--------+
    |    | Var    |
Out-+    |        +-Out
    |    | Ret    |
    +----+----+---+
              | Out

# TyArr (3 x 3)

            A
            |
          +-+-+
(A -> B)--+###+--B
          +-+-+
            |
            B

   (B is either one, not both)

-}

isAbsVar, isAbsTy, isAbsRet, isAbsOut,
  isAppArg, isAppFun, isAppOut,
  isTyArrA, isTyArrB, isTyArrOut ::
    (Direction, Toward, ccid) -> Maybe ccid
isAbsVar (dir, tow, cid) = cid <$ guard (dir == North && tow == ToInside)
isAbsRet (dir, tow, cid) = cid <$ guard (dir == South && tow == ToInside)
isAbsTy  (dir, tow, cid) = cid <$ guard (dir == South && tow == ToOutside)
isAbsOut (dir, tow, cid) = cid <$ guard (dir /= South && tow == ToOutside)

isAppArg (dir, tow, cid) = cid <$ guard (dir == South && tow == ToOutside)
isAppFun (dir, tow, cid) = cid <$ guard (dir == East && tow == ToOutside)
isAppOut (dir, tow, cid) = cid <$ guard ((dir == North || dir == West) && tow == ToOutside)

isTyArrA (dir, tow, cid) = cid <$ guard (dir == South && tow == ToOutside)
isTyArrOut (dir, tow, cid) = cid <$ guard (dir == East && tow == ToOutside)
isTyArrB (dir, tow, cid) = cid <$ guard ((dir == North || dir == West) && tow == ToOutside)

mkAbsBox, mkAppBox, mkTyAbsBox, mkTyArrBox
  :: LBPosition -> [(Direction, Toward, CCID)] -> Either [LBError] BoxAttr

mkAbsBox pos conns = do
   ret <- case rets of
     [ret] -> pure ret
     _     -> failure pos $ "AbsBox - Number of return wires " ++ show (length rets) ++ " /= 1"
   ty <- case tys of
     [ty] -> pure ty
     _    -> failure pos $ "AbsBox - Number of type wires " ++ show (length tys) ++ " /= 1"
   pure $ AbsBox { _boxVar = vars, _boxRet = ret, _boxTy = ty, _boxOut = outs }
   where
    vars = mapMaybe isAbsVar conns
    tys  = mapMaybe isAbsTy conns
    rets = mapMaybe isAbsRet conns
    outs = mapMaybe isAbsOut conns

mkAppBox pos conns = do
    fun <- case funs of
        [fun] -> pure fun
        []    -> failure pos "AppBox - No funs"
        _     -> failure pos "AppBox - Many funs (?)"
    arg <- case args of
        [arg] -> pure arg
        []    -> failure pos "AppBox - No args"
        _     -> failure pos "AppBox - Many args (?)"
    pure $ AppBox { _boxFun = fun, _boxArg = arg, _boxOut = outs }
  where
    funs = mapMaybe isAppFun conns
    args = mapMaybe isAppArg conns
    outs = mapMaybe isAppOut conns

mkTyAbsBox pos conns = do
   ret <- case rets of
     [ret] -> pure ret
     _     -> failure pos $ "TyAbsBox - Number of return wires " ++ show (length rets) ++ " /= 1"
   pure $ TyAbsBox { _boxTyVar = vars, _boxRet = ret, _boxOut = outs }
   where
    vars = mapMaybe isAbsVar conns
    rets = mapMaybe isAbsRet conns
    outs = mapMaybe isAbsOut conns
mkTyArrBox pos conns = do
    case funs of
        [_] -> pure ()
        []  -> failure pos "TyArrBox - No funs"
        _   -> failure pos "TyArrBox - Many funs (?)"
    a <- case as of
        [a] -> pure a
        []  -> failure pos "TyArrBox - No a"
        _   -> failure pos "TyArrBox - Many a (?)"
    b <- case bs of
        [b] -> pure b
        []  -> failure pos "TyArrBox - No b"
        _   -> failure pos "TyArrBox - Many b"
    pure $ TyArrBox { _boxTyA = a, _boxTyB = b, _boxOut = funs }
  where
    funs = mapMaybe isTyArrOut conns
    as = mapMaybe isTyArrA conns
    bs = mapMaybe isTyArrB conns

failure :: LBPosition -> String -> Either [LBError] a
failure pos = Left . pure .  LBError pos

mkWire :: CCWire -> Either [LBError] Wire
mkWire dat
  | _hasLoop dat = Left [LBError pos "Looped Wire"]
  | otherwise = Right $ _lines dat
  where
    pos = LBPosLines "" $ _lines dat

uniqueOutput :: Diagram -> Validation [LBError] CCID
uniqueOutput Diagram{ _diagramWires = wires, _diagramOutWires = outgoings } =
  case outgoings of
    [] -> err "Output is not specified"
    [wid] -> pure wid
    _ -> err $ "Multiple outputs are specified: " ++ format (_lines . (wires Map.!) <$> outgoings)
  where
    err = Failure . pure . LBError (LBPosStr "TopLevel")


-- * Main routine

mkPreExpr :: Diagram -> Either [LBError] (PreExpr VarName)
mkPreExpr diag = do
  (wires, boxes, out) <- validationToEither $
    (,,)
      <$ checkNoIntersectingBoxes (_diagramBoxes diag)
      <*> traverse (eitherToValidation . mkWire) (_diagramWires diag)
      <*> traverse (eitherToValidation . mkBox) (_diagramBoxes diag)
      <*> uniqueOutput diag
  validationToEither $ checkAssignOnce wires boxes
  pure $ buildPreExpr boxes out

checkNoIntersectingBoxes :: Map CCID CCBox -> Validation [LBError] ()
checkNoIntersectingBoxes boxes = case inters of
  [] -> pure ()
  _  -> Failure $ map interErr inters
  where
    rects = _bb <$> boxes
    inters = intersectingBoxes rects
    interErr (i,j) = LBError (LBPosBox "" iBox) $ "Intersecting boxes:" ++ format jBox
      where
        iBox = rects ! i
        jBox = rects ! j

checkAssignOnce :: Map CCID Wire -> Map CCID (Rect, BoxAttr) -> Validation [LBError] ()
checkAssignOnce wires boxes = traverse_ checkTooManyError . groupByWire $ allOutputs
  where
    allOutputs =
      [ (wid, box)
         | (box, attr) <- Map.elems boxes,
           wid <- definingWires attr
      ]
    groupByWire = groupBy ((==) `on` fst) . sortBy (compare `on` fst)
    checkTooManyError grp = case grp of
      (wid, _) : _ : _ ->
        let wirePos = LBPosLines "" (wires ! wid)
            err = "Multiple output to one wire, from: " ++ format (map snd grp)
         in Failure [LBError wirePos err]
      _ -> pure ()

definingWires :: BoxAttr -> [CCID]
definingWires attr = case attr of
  AbsBox{ _boxOut = outs, _boxVar = vars } -> outs ++ vars
  TyAbsBox{ _boxOut = outs, _boxTyVar = vars } -> outs ++ vars
  AppBox{ _boxOut = outs } -> outs
  TyArrBox{ _boxOut = outs } -> outs

buildPreExpr :: Map CCID (Rect, BoxAttr) -> CCID -> PreExpr VarName
buildPreExpr boxes = goLet [] toplevel
  where
    children = directChildrenRel $ fst <$> boxes
    toplevel = rootBoxes $ fst <$> boxes

    getChildren :: CCID -> [CCID]
    getChildren bid = Set.toList $ Rel.lookupL bid children

    goLet ::
         [(VarName, PreExpr VarName)]
      -> [CCID]
      -> CCID
      -> PreExpr VarName
    goLet otherDefs childrensId ret = PreLet (otherDefs ++ subDefs) (W ret)
      where
        subDefs = childrensId >>= goBox

    boxDefs bid expr outs = (B bid, expr) : [ (W y, PreVar (B bid)) | y <- outs ]

    goBox :: CCID -> [(VarName, PreExpr VarName)]
    goBox bid = case snd (boxes ! bid) of
        AbsBox{ _boxOut = outs, _boxTy = ty, _boxVar = vars, _boxRet = ret } ->
          let arg = A bid
              argDefs = [ (W x, PreVar arg) | x <- vars ]
              body = goLet argDefs (getChildren bid) ret
              expr = PreAbs arg (PreVar (W ty)) body
          in boxDefs bid expr outs
        TyAbsBox{ _boxOut = outs, _boxTyVar = vars, _boxRet = ret } ->
          let arg = A bid
              argDefs = [ (W x, PreVar arg) | x <- vars ]
              body = goLet argDefs (getChildren bid) ret
              expr = PreTyAbs arg body
          in boxDefs bid expr outs
        AppBox{ _boxOut = outs, _boxFun = fun, _boxArg = arg } ->
          let expr = PreApp (PreVar (W fun)) (PreVar (W arg))
          in boxDefs bid expr outs
        TyArrBox{ _boxOut = outs, _boxTyA = a, _boxTyB = b} ->
          let expr = PreTyArr (PreVar (W a)) (PreVar (W b))
          in boxDefs bid expr outs

-- | Eliminate "var = var" definitions
simplPreExpr :: Ord x => PreExpr x -> PreExpr x
simplPreExpr = go Map.empty
  where
    isSimple (x, PreVar y) = Right (x,y)
    isSimple (x, e) = Left (x, e)

    go subst e = case e of
      PreVar x -> PreVar (subst @@ x)
      PreApp e1 e2 -> PreApp (go subst e1) (go subst e2)
      PreTyArr e1 e2 -> PreTyArr (go subst e1) (go subst e2)
      PreAbs x t body -> PreAbs x (go subst t) (go (Map.delete x subst) body)
      PreTyAbs x body -> PreTyAbs x (go (Map.delete x subst) body)
      PreLet defs ret ->
        let xs = Set.fromList (fst <$> defs)
            subst' = subst `Map.withoutKeys` xs
            (complexDefs, simpleDefs) = partitionEithers (isSimple <$> defs)
            subst'' = foldl' addDef subst' simpleDefs
            defs' = [ (x, go subst'' rhs) | (x, rhs) <- complexDefs ]
            ret' = subst'' @@ ret
        in if null defs'
              then PreVar ret'
              else PreLet defs' ret'

(@@) :: Ord x => Map.Map x x -> x -> x
subst @@ x = Map.findWithDefault x x subst

addDef :: Ord x => Map.Map x x -> (x,x) -> Map.Map x x
addDef subst (x,y) = subst'
  where
    y' = subst @@ y
    f z = if x == z then y' else z
    subst' | x == y' = subst
           | otherwise = Map.insert x y' (Map.map f subst)
