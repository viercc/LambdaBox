{-# LANGUAGE FlexibleContexts #-}
module LambdaBox.Typed.ImageParser
  ( preprocess,
    mkDiagram,
    parse,
    parseFromRGBA,
    parseFile,
    parseFileDebug,
    parseMemory
  )
where

-- import Debug.Trace

import Prelude hiding (lines)
import Data.Word (Word8)

import Codec.Picture
import Codec.Picture.Types (pixelFold)
import qualified Data.ByteString as BS
import qualified Data.PointSet as PS

import Geom (Point(..))
import LambdaBox.Error
import LambdaBox.Image

import LambdaBox.Diagram
import LambdaBox.Typed.ImageParser.PreExpr
import LambdaBox.Typed.ImageParser.Expr
import qualified LineImage
import Text.PrettyPrint.Formattable (format)

--------------------------------------------------------
--  Main Logic
--------------------------------------------------------

-- | Preprocess image for 'parse'
preprocess :: Image PixelRGBA8 -> Image Pixel8
preprocess = pixelMap isBlack
  where
    isBlack :: PixelRGBA8 -> Pixel8
    isBlack (PixelRGBA8 r g b _) = if r == 0 && g == 0 && b == 0 then 1 else 0

-- | Turn image into set of 'Point's
toPointSet :: Image Word8 -> PS.PointSet
toPointSet image = PS.fromList $ pixelFold f [] image
  where
    f accPoints x y pixValue
      | pixValue /= 0 = Point x y : accPoints
      | otherwise = accPoints

mkDiagram :: Image Word8 -> Diagram
mkDiagram image = fromLineImage (LineImage.fromPointSet (toPointSet image))

-- | Parse image to circuit
parse :: Image Pixel8 -> Path -> Either [LBError] Expr'
parse image path = setPath path $ buildExpr $ mkDiagram image

-- | preprocess and then parse
parseFromRGBA :: Image PixelRGBA8 -> Path -> Either [LBError] Expr'
parseFromRGBA = parse . preprocess

-- | read from file and then parse
parseFile :: Path -> IO (Either [LBError] Expr')
parseFile path = do
  readResult <- readImageRGBA8 path
  return $
    case readResult of
      Left mes -> Left [LBError noPosition mes]
      Right rgbaImage -> parseFromRGBA rgbaImage path

parseFileDebug :: Path -> IO (Either [LBError] Expr')
parseFileDebug path = do
    Right rgbaImage <- readImageRGBA8 path
    let binaryImage = preprocess rgbaImage
        pointSet = toPointSet binaryImage
        lineImage = LineImage.fromPointSet pointSet
        diag = LambdaBox.Diagram.fromLineImage lineImage
    putStrLn "==   Begin debug dump   =="
    putStrLn "---- LineImage ----"
    putStrLn $ format lineImage
    putStrLn "---- Diagram ----"
    print diag
    putStrLn "---- PreExpr ----"
    result <- case mkPreExpr diag of
      Left err -> do
        putStrLn $ format err
        pure (Left err)
      Right preExpr -> do
        putStrLn $ format preExpr
        putStrLn "---- PreExpr (simpl) ----"
        let preExpr' = simplPreExpr preExpr
        putStrLn $ format preExpr'
        putStrLn "---- Expr ----"
        let expr = buildFromPreExpr diag preExpr'
        putStrLn $ either format format expr
        pure expr
    putStrLn "==   End debug dump   =="
    pure (setPath path result)

parseMemory :: Path -> BS.ByteString -> Either [LBError] Expr'
parseMemory path bs =
  let readResult = decodeImageRGBA8 bs
   in case readResult of
        Left mes -> Left [LBError noPosition mes]
        Right rgbaImage -> parseFromRGBA rgbaImage path

-- | Checks parsed expression has error

setPathToError :: Path -> LBError -> LBError
setPathToError path (LBError pos mes) = LBError (posSetPath path pos) mes

setPath :: Path -> Either [LBError] Expr' -> Either [LBError] Expr'
setPath path (Left errors) = Left $ map (setPathToError path) errors
setPath _    (Right expr) = Right expr
