{-# LANGUAGE RankNTypes #-}
module LambdaBox.Typed.Eval.Types where

import Bound.Scope
import Bound.Var (Var (..))
import LambdaBox.Typed.Expr.DeBruijn
import Numeric.Natural (Natural)

-- Common --
($->) :: Type a b -> Type a b -> Type a b
($->) = TyArr

infixr 3 $->

mkTyForall :: a -> (forall b'. Type a b' -> Type a b') -> Type a b
mkTyForall a buildTy = TyForall a $ abstractEither id $ buildTy (TyVar (Left ()))

data EvalFailure = UnexpectedHead
   deriving (Eq, Show)

-- * Types

boolType :: Type String b
boolType = mkTyForall "a" $ \a -> a $-> a $-> a

natType :: Type String b
natType = mkTyForall "a" $ \a -> (a $-> a) $-> a $-> a

progType :: Type String b
progType = mkTyForall "a" $ \a ->
      (a $-> a $-> a $-> a)
  $-> (a $-> a)
  $-> (a $-> a)
  $-> a
  $-> a

-- * test values

idExpr :: ClosedTerm String String
idExpr = LamTy "a" $ Lam "x" a $ toScope $ Var (B ())
  where a = TyVar (B ())

maltypedExpr :: ClosedTerm String String
maltypedExpr = LamTy "a" $ Lam "x" a $ toScope $ App (Var (B ())) (Var (B ()))
  where a = TyVar (B ())

boolExpr :: Bool -> ClosedTerm String String
boolExpr b = LamTy "a" $ Lam "x" a $ toScope $ Lam "y" a $ toScope body
  where
    a = TyVar (B ())
    body = if b then Var (F (B ())) else Var (B ())

loopExpr :: ClosedTerm String String
loopExpr = LamTy "a" $ LetRec [ ("bot", toScope (Assert (Var (B 0)) (TyVar (B ())))) ] (toScope (Var (B 0)))

natExpr :: Natural -> ClosedTerm String String
natExpr n = LamTy "a" $ Lam "s" (a $-> a) $ toScope $ Lam "z" a $ toScope $ gen n
  where
    a = TyVar (B ())
    gen 0 = Var (B ())
    gen k = App (Var (F (B ()))) (gen (k-1))

natPowExpr :: ClosedTerm String String
natPowExpr =
  Lam "n" natType $ toScope $
  Lam "m" natType $ toScope $
  LamTy "a" $ AppTy m (a $-> a) `App` AppTy n a
  where
    a = TyVar (B ())
    n = Var (F (B ()))
    m = Var (B ())

nat256Expr :: ClosedTerm String String
nat256Expr = natPowExpr `App` natExpr 2 `App` (natPowExpr `App` natExpr 2 `App` natExpr 3)
