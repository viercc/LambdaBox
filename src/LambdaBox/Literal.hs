{-# LANGUAGE OverloadedStrings #-}

module LambdaBox.Literal
  ( Literal (..),
  )
where

import Prettyprinter (Pretty (..), viaShow)
import Text.PrettyPrint.Formattable

data Literal
  = IntLit !Int
  | BoolLit !Bool
  | CharLit !Char
  | InputStrLit String
  | ListLit [Literal]
  deriving (Show, Read)

instance Formattable Literal where
  formatPrec _ (IntLit n) = pretty n
  formatPrec _ (BoolLit b) = if b then "$T" else "$F"
  formatPrec _ (CharLit c) = viaShow c
  formatPrec _ (InputStrLit str) = viaShow str
  formatPrec _ (ListLit lits) = formatList lits
