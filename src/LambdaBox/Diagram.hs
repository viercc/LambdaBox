{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RankNTypes #-}

module LambdaBox.Diagram
  ( Diagram(..),
    CCID (..),
    CCBox (..),
    BoxType(..),
    Direction (..),
    Toward (..),
    CCWire (..),
    fromLineImage,

    extractBoxes
  )
where

import Control.Monad (guard, void)
import qualified Data.DList as DL
import Data.Foldable
import Data.List (partition)
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as Map
import qualified Data.Set as Set

import Data.Equivalence.Monad

import Geom
import Geom.Rect
import Geom.Line
import LineImage

import Text.PrettyPrint.Formattable
import Geom.RectUtil (maximalBoxes)

-- | Boxes-and-Wires diagram
data Diagram = Diagram {
        _diagramBoxes :: Map CCID CCBox,
        _diagramWires :: Map CCID CCWire,
        _diagramOutWires :: [CCID]
    }
    deriving (Show, Eq, Ord)

newtype CCID = MkCCID Int
  deriving newtype (Eq, Ord, Show, Formattable)

data CCBox = CCBox
  { _bb :: Rect,
    _boxType :: BoxType,
    _connects :: [(Direction, Toward, CCID)]
  }
  deriving (Show, Eq, Ord)

{- |

* Square

  >   3
  > ■■■
  > ■□■ 3
  > ■■■

* FilledSquare

  >   3
  > ■■■
  > ■■■ 3
  > ■■■

* ThinBox

  >       N
  > ■■■■■■
  > ■□□□□■
  > ■□□□□■ M
  > ■□□□□■
  > ■■■■■■


* ThickBox

  >       N
  > ■■■■■■
  > ■■■■■■
  > ■□□□□■ M
  > ■□□□□■
  > ■■■■■■

-}
data BoxType = Square | FilledSquare | ThinBox | ThickBox
  deriving (Eq, Ord, Show, Read)

data Toward = ToInside | ToOutside
  deriving (Eq, Ord, Show, Read)

instance Formattable BoxType
instance Formattable Toward

data CCWire = CCWire {_lines :: [Line], _hasLoop :: Bool}
  deriving (Show, Eq, Ord)

----------------------------------------------------------------

fromLineImage :: LineImage -> Diagram
fromLineImage im = Diagram {
      _diagramBoxes = boxesMap,
      _diagramWires = wiresMap,
      _diagramOutWires = outgoings
      }
  where
    (rects, im') = extractBoxes im
    (boxes, im'') = processBlackBoxes rects im'
    wires = twistWires im''
    (boxesMap, wiresMap, revWiresMap) = assemble im'' boxes wires

    outgoingLines = findLowestLines im''
    maxes = maximalBoxes rects
    outgoings = Set.toList $ Set.fromList $
      [ revWiresMap Map.! l 
        | l <- outgoingLines
        , all (\b -> not (b `contains` boundingBox l)) maxes ]

extractBoxes :: LineImage -> ([Rect], LineImage)
extractBoxes image0 = loop image0 (getHorizontalLines image0)
  where
    loop :: LineImage -> [Line] -> ([Rect], LineImage)
    loop im [] = ([], im)
    loop im (topLine:restLines) =
      let vs = [ (dir, v) | (Corner dir, v) <- intersectionsWithRel topLine im ]
          maybeBox = do
            (West, v1@(Vertical x1 ys)) <- vs
            (North, v2@(Vertical x2 ys')) <- vs
            guard (ys == ys')
            let Range y1 y2 = ys
                bottomLine = Horizontal (Range x1 x2) y2
            guard (bottomLine `member` im)
            let im' = deleteLine topLine . deleteLine v1 . deleteLine v2 . deleteLine bottomLine $ im
            pure (Rect x1 y1 x2 y2, im')
      in case maybeBox of
           [] -> loop im restLines
           (box, im') : _ -> case loop im' restLines of
              ~(boxes, finalIm) -> (box : boxes, finalIm)

is3x3Box :: Rect -> Bool
is3x3Box box = rectWidth box == 3 && rectHeight box == 3

consFst :: a -> ([a],b) -> ([a],b)
consFst a ~(as,b) = (a:as, b)

processBlackBoxes :: [Rect] -> LineImage -> ([(Rect, BoxType)], LineImage)
processBlackBoxes [] im = ([], im)
processBlackBoxes (box:boxes) im
   | not (is3x3Box box) =
       let secondTopLine = Horizontal (xRange box) (succ (rectTop box))
       in if secondTopLine `member` im
            then consFst (box, ThickBox) (processBlackBoxes boxes (removeSecondTopLine secondTopLine im))
            else consFst (box, ThinBox) (processBlackBoxes boxes im)
   | otherwise = 
       let center = Point (rectLeft box + 1) (rectTop box + 1)
           linesOnCenter = lookupLinesAt center im
           im' = splitLinesAt center linesOnCenter im
       in if null linesOnCenter
            then consFst (box, Square) (processBlackBoxes boxes im)
            else consFst (box, FilledSquare) (processBlackBoxes boxes im')

splitLinesAt :: Point -> [Line] -> LineImage -> LineImage
splitLinesAt (Point x y) ls im0 = foldl' step im0 ls
  where
    step im line = foldl' (flip insertLine) (deleteLine line im) (splitLine line)

    isLong (Range a1 a2) = a2 - a1 + 1 >= 2
    
    splitLine line = case line of
        Horizontal (Range x1 x2) _ ->
            let newXs = filter isLong [ Range x1 (pred x), Range (succ x) x2 ]
            in (`Horizontal` y) <$> newXs
        Vertical _ (Range y1 y2) ->
            let newYs = filter isLong [ Range y1 (pred y), Range (succ y) y2 ]
             in Vertical x <$> newYs

removeSecondTopLine :: Line -> LineImage -> LineImage
removeSecondTopLine secondTop@(Horizontal (Range x1 x2) y2) im =
    let im' = deleteLine secondTop im
        ys = Range (pred y2) y2
        shortVerticalLines = [ Vertical x ys | x <- [x1 .. x2] ]
    in foldl' (flip deleteLine) im' shortVerticalLines
removeSecondTopLine _ _ = error "Must be horizontal"

twistWires :: LineImage -> [Set.Set Line]
twistWires im = runEquivM DL.singleton (<>) $ do
  -- Instantiate every lines at the beginning
  for_ (getLines im) (void . classDesc)

  -- Walk for all connections
  for_ (getHorizontalLines im) $ \line ->
    for_ (intersectionsWithRel line im) $ \(rel, line') ->
      case rel of
        Cross -> pure ()
        _     -> equate line line'
  
  allDescs <- classes >>= traverse desc
  pure $ map (Set.fromList . DL.toList) allDescs

assemble :: LineImage -> [(Rect, BoxType)] -> [Set.Set Line] -> (Map CCID CCBox, Map CCID CCWire, Map Line CCID)
assemble im boxes wires = (Map.map mkBox boxesMap, Map.map mkWire wiresMap, reverseMap)
  where
    numBoxes = length boxes
    boxIds = MkCCID <$> [0..numBoxes - 1]
    wireIds = MkCCID <$> [numBoxes ..]
    boxesMap = Map.fromDistinctAscList (zip boxIds boxes)
    wiresMap = Map.fromDistinctAscList (zip wireIds wires)
    
    reverseMap = Map.unions [ Map.fromSet (const i) wire | (i,wire) <- Map.toList wiresMap ]
    
    mkWire wire = CCWire (Set.toList wire) (hasLoop (Set.toList wire))
    mkBox (box, boxType) = CCBox box boxType (findConnections box)

    findConnections box =
      do edge <- rectBorders box
         (Tee dir, line) <- intersectionsWithRel edge im
         let (_, towards, _) = judgeTowards dir box line
         pure (dir, towards, reverseMap Map.! line)

findLowestLines :: LineImage -> [Line]
findLowestLines im = outgoings
  where
    vs = getVerticalLines im
    lowestPoint = [ maximum [ y2 | Vertical _ (Range _ y2) <- vs ] | not (null vs) ]
    outgoings =
      [ l | maxY <- lowestPoint
          , l@(Vertical _ (Range _ y2)) <- vs
          , y2 == maxY ]

hasLoop :: [Line] -> Bool
hasLoop ls = runEquivM' (loop (allIntersections ls))
  where
    loop [] = return False
    loop ((thisLine, thatLine) : rest) =
      do
        alreadyEq <- equivalent thisLine thatLine
        if alreadyEq
          then return True
          else equate thisLine thatLine >> loop rest

allIntersections :: [Line] -> [(Line, Line)]
allIntersections ls =
  [(h, v) | h <- hs, v <- vs, isTouching (lineRelation h v)]
  where
    (hs, vs) = partition isHorizontal ls
    isHorizontal Horizontal {} = True
    isHorizontal _ = False

    isTouching Nothing = False
    isTouching (Just rel) = rel /= Cross

judgeTowards :: Direction -> Rect -> Line -> (Direction, Toward, Line)
judgeTowards dir thisRect thatLine = (dir, towards, thatLine)
  where
    towards = case rectIntersection thisRect (boundingBox thatLine) of
      Just rr | notSingleDot rr -> ToInside
      _ -> ToOutside
    notSingleDot rr = rectWidth rr > 1 || rectHeight rr > 1
