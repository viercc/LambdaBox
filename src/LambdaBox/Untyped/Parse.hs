module LambdaBox.Untyped.Parse where

import Control.Applicative
import Data.Lambda.Parse
import Data.Lambda.Parse.Custom
import Data.VarName
import LambdaBox.Error
import LambdaBox.Untyped.Expr
import LambdaBox.Literal
import Prettyprinter (Pretty (..))
import qualified Text.Megaparsec as P
import qualified Text.Megaparsec.Char as P
import qualified Text.Megaparsec.Char.Lexer as L

parseLiteral :: Parser Literal
parseLiteral =
  IntLit <$> intLiteral
    <|> BoolLit <$> boolLiteral
    <|> CharLit <$> charLiteral
    <|> InputStrLit <$> stringLiteral
    <|> ListLit <$> listOf parseLiteral

space :: Parser ()
space = L.space P.space1 (L.skipLineComment "#") empty

lexeme :: Parser a -> Parser a
lexeme = L.lexeme space

symbol :: String -> Parser String
symbol = L.symbol space

boolLiteral :: Parser Bool
boolLiteral = lexeme $ P.char '$' *> (False <$ P.char 'F' <|> True <$ P.char 'T')

charLiteral :: Parser Char
charLiteral = lexeme $ P.char '\'' *> L.charLiteral <* P.char '\''

stringLiteral :: Parser String
stringLiteral = lexeme $ P.char '"' *> P.manyTill L.charLiteral (P.char '"')

intLiteral :: Parser Int
intLiteral = lexeme $ L.signed (return ()) L.decimal

listOf :: Parser a -> Parser [a]
listOf p = P.between (symbol "[") (symbol "]") $ P.sepBy p (symbol ",")

lamP' :: Parser LBExpr
lamP' = mapLoc (LBPosStr . show . pretty) <$> lamP parseLiteral

varP :: Parser VarName
varP = Data.Lambda.Parse.Custom.varP (noExtension defaultPreParser)
