module LambdaBox.Untyped.Expr.LamUtil where

import Data.Lambda.Core

abstractEither :: (x -> Either Int x') -> Lam v l a x -> Scope (Lam v l a x')
abstractEither handleFV = Scope . foldWithDepth step
  where
    step d fe = case viewFV fe of
      Right y -> case handleFV y of
        Left i  -> bv_ d i
        Right y' -> fv_ y'
      Left (BV n m) | n >= d    -> bv_ (n+1) m
                    | otherwise -> bv_ n m
      Left fe' -> Lam fe'
