module LambdaBox.Untyped.Expr
  ( module Data.Lambda,
    module Data.VarName,
    module Data.Lambda.Core,
    LBExpr, LBExpr_,
    varE,
    appE,
    absE,
    letE,
    litE,
  )
where

import Data.Lambda
import Data.Lambda.Core
import Data.VarName
import LambdaBox.Error
import LambdaBox.Literal

type LBExpr = LBExpr_ VarName
type LBExpr_ = Lam VarName LBPosition Literal

varE :: LBPosition -> VarName -> LBExpr
varE pos v = loc_ pos (fv_ v)

appE :: LBPosition -> LBExpr -> LBExpr -> LBExpr
appE pos e1 e2 = loc_ pos (app_ e1 e2)

absE :: LBPosition -> VarName -> LBExpr -> LBExpr
absE pos x body = loc_ pos (makeAbs x body)

letE :: LBPosition -> [(VarName, LBExpr)] -> LBExpr -> LBExpr
letE _ [] body = body
letE pos defs body = loc_ pos (makeLetRec defs body)

litE :: LBPosition -> Literal -> LBExpr
litE pos lit = loc_ pos (lit_ lit)
