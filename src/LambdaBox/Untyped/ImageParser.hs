{-# LANGUAGE FlexibleContexts #-}

module LambdaBox.Untyped.ImageParser
  ( preprocess,
    parse,
    parseFromRGBA,
    parseFile,
    parseMemory,
    checkError,
  )
where

-- import Debug.Trace

import Prelude hiding (lines)
import Data.Word (Word8)

import Codec.Picture
import Codec.Picture.Types (pixelFold)
import qualified Data.ByteString as BS
import qualified Data.PointSet as PS

import Geom (Point(..))
import LambdaBox.Error
import LambdaBox.Untyped.Expr
import LambdaBox.Image
import LambdaBox.Untyped.ImageParser.BuildExpr
import qualified LineImage
import qualified LambdaBox.Diagram


--------------------------------------------------------
--  Main Logic
--------------------------------------------------------

-- | Preprocess image for 'parse'
preprocess :: Image PixelRGBA8 -> Image Pixel8
preprocess = pixelMap isBlack
  where
    isBlack :: PixelRGBA8 -> Pixel8
    isBlack (PixelRGBA8 r g b _) = if r == 0 && g == 0 && b == 0 then 1 else 0

-- | Turn image into set of 'Point's
toPointSet :: Image Word8 -> PS.PointSet
toPointSet image = PS.fromList $ pixelFold f [] image
  where
    f accPoints x y pixValue
      | pixValue /= 0 = Point x y : accPoints
      | otherwise = accPoints

-- | Parse image to circuit
parse :: Image Pixel8 -> Path -> Either [LBError] LBExpr
parse image path = setPath path mayExpr
  where
    mayExpr = do
      let parts = LambdaBox.Diagram.fromLineImage (LineImage.fromPointSet (toPointSet image))
      buildExpr parts

-- | preprocess and then parse
parseFromRGBA :: Image PixelRGBA8 -> Path -> Either [LBError] LBExpr
parseFromRGBA = parse . preprocess

-- | read from file and then parse
parseFile :: Path -> IO (Either [LBError] LBExpr)
parseFile path = do
  readResult <- readImageRGBA8 path
  return $
    case readResult of
      Left mes -> Left [LBError noPosition mes]
      Right rgbaImage -> parseFromRGBA rgbaImage path

parseMemory :: Path -> BS.ByteString -> Either [LBError] LBExpr
parseMemory path bs =
  let readResult = decodeImageRGBA8 bs
   in case readResult of
        Left mes -> Left [LBError noPosition mes]
        Right rgbaImage -> parseFromRGBA rgbaImage path

-- | Checks parsed expression has error
checkError :: LBExpr -> Either [LBError] LBExpr
checkError expr =
  let errors = checkUndefinedVar expr
   in if null errors then Right expr else Left errors

setPathToError :: Path -> LBError -> LBError
setPathToError path (LBError pos mes) = LBError (posSetPath path pos) mes

setPathToExpr :: Path -> LBExpr -> LBExpr
setPathToExpr path = mapLoc (posSetPath path)

setPath :: Path -> Either [LBError] LBExpr -> Either [LBError] LBExpr
setPath path (Left errors) = Left $ map (setPathToError path) errors
setPath path (Right expr) = Right $ setPathToExpr path expr
