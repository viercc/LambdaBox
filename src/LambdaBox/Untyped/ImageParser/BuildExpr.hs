{-# LANGUAGE TupleSections #-}

module LambdaBox.Untyped.ImageParser.BuildExpr
  ( buildExpr,
    checkUndefinedVar,
  )
where

-- import Debug.Trace

import Prelude hiding (lines)
import Data.Foldable (fold)
import Data.Function (on)
import Data.List (groupBy, sortBy)

import Data.Map.Lazy ((!), Map)
import qualified Data.Map.Lazy as Map
import Text.PrettyPrint.Formattable
import Data.Either.Validation

import Geom.Rect
import Geom.Line
import Geom.RectUtil

import LambdaBox.Error
import LambdaBox.Untyped.Expr
import LambdaBox.Diagram
import qualified Data.Relation as Rel
import qualified Data.Set as Set
import Control.Monad (guard)
import Data.Maybe (mapMaybe)

------------------------------------------------------------
--  Circuit to Expr
------------------------------------------------------------
buildExpr :: Diagram -> Either [LBError] LBExpr
buildExpr diag = do
  (wires, boxes, out) <- validationToEither $
    (,,)
      <$  checkNoIntersectingBoxes (_diagramBoxes diag)
      <*> traverse (eitherToValidation . toWire) (_diagramWires diag)
      <*> traverse (eitherToValidation . toBox) (_diagramBoxes diag)
      <*> uniqueOutput diag
  case checkAssignOnce wires boxes of
    []   -> pure ()
    errs -> Left errs
  pure $ circuitToExpr wires boxes out

circuitToExpr :: Map CCID Wire -> Map CCID Box -> CCID -> LBExpr
circuitToExpr wires boxes topOut = exprTop
  where
    childrenRel = directChildrenRel (fst <$> boxes)
    children bid = Set.toList $ Rel.lookupL bid childrenRel
    toplevel = rootBoxes (fst <$> boxes)

    makeVarName :: CCID -> VarName
    makeVarName ccid = VarName $ "x" ++ show ccid

    makeBoxVarName :: CCID -> VarName
    makeBoxVarName ccid = VarName $ "t" ++ show ccid

    wirePos wid = LBPosLines "" (wires ! wid)
    wireVarE wid = varE (wirePos wid) (makeVarName wid)

    bid <== expr = (makeVarName bid, expr)
    
    toDefs bid = (tmpVar, expr) : map (<== tmpVarE) outs
      where
        (bb, attr) = boxes ! bid
        pos = LBPosBox "" bb
        outs = _boxOut attr
        tmpVar = makeBoxVarName bid
        tmpVarE = varE pos tmpVar
        expr = case attr of
          AbsBox _ vars ret ->
            let argVar = makeVarName bid
                body = letE pos lets (wireVarE ret)
                lets = defsArg ++ defsChildren
                defsArg = map (<== varE pos argVar) vars
                defsChildren = children bid >>= toDefs
            in absE pos argVar body
          AppBox _ fun arg -> appE pos (wireVarE fun) (wireVarE arg)

    exprTop = letE pos defs topVarE
      where
        pos = LBPosStr "TopLevel"
        defs = toplevel >>= toDefs
        topVar
          | topOut `Map.member` boxes = makeBoxVarName topOut
          | otherwise                 = makeVarName topOut
        topVarE = varE pos topVar

checkAssignOnce :: Map CCID Wire -> Map CCID Box -> [LBError]
checkAssignOnce wires boxes = errors
  where
    allOutputs = Map.elems boxes >>= boxOutputs
    boxOutputs (rect, AppBox out _ _) = fmap (, rect) out
    boxOutputs (rect, AbsBox out var _) = fmap (, rect) (out ++ var)
    groupByWire = groupBy ((==) `on` fst) . sortBy (compare `on` fst)
    tooMany xs = length xs >= 2
    toErrors = fmap toError
    toError (wid, rect) =
      let boxPos = LBPosBox "" rect
          wirePos = LBPosLines "" (wires ! wid)
       in LBError boxPos $ "multiple output to wire:" ++ format wirePos

    errors = concatMap toErrors . filter tooMany . groupByWire $ allOutputs

checkUndefinedVar :: LBExpr -> [LBError]
checkUndefinedVar expr = foldWithLoc step expr noPosition
  where
    step pos fe = case fe of
      FV x -> [LBError pos ("Undefined variable:" ++ format x)]
      _ -> fold fe

------------------------

data BoxAttr
  = AbsBox
      { _boxOut :: ![CCID],
        _boxVar :: ![CCID],
        _boxRet :: !CCID
      }
  | AppBox
      { _boxOut :: ![CCID],
        _boxFun :: !CCID,
        _boxArg :: !CCID
      }
  deriving (Show, Eq)

type Box = (Rect, BoxAttr)

type Wire = [Line]

toBox :: CCBox -> Either [LBError] Box
toBox dat = (rect, ) <$> attr
  where
    rect = _bb dat
    conns = _connects dat
    pos = LBPosBox "" rect

    attr = case _boxType dat of
      Square -> mkAppBox pos conns
      ThinBox -> mkAbsBox pos conns
      otherType -> Left [LBError pos $ "Not a box for untyped LambdaBox:" ++ show otherType ]

{-  # Abs
          Var | Out
      +-------+
      |   |   |
  Out-|   |   |-Out
      |   |   |
      +-------+
          Ret | Out

    # App
          Arg
          |
        +---+
    Fun-|3x3|-Out
        +---+
          |
          Out            -}
isAbsVar,
  isAbsRet,
  isAbsOut,
  isAppArg,
  isAppFun,
  isAppOut ::
    (Direction, Toward, ccid) -> Maybe ccid
isAbsVar (dir, tow, cid) = cid <$ guard (dir == North && tow == ToInside)
isAbsRet (dir, tow, cid) = cid <$ guard (dir == South && tow == ToInside)
isAbsOut (_, tow, cid) = cid <$ guard (tow == ToOutside)

isAppArg (dir, tow, cid) = cid <$ guard (dir == South && tow == ToOutside)
isAppFun (dir, tow, cid) = cid <$ guard (dir == East && tow == ToOutside)
isAppOut (dir, tow, cid) = cid <$ guard ((dir == North || dir == West) && tow == ToOutside)

mkAppBox, mkAbsBox :: LBPosition -> [(Direction, Toward, CCID)] -> Either [LBError] BoxAttr

mkAppBox pos conns = do
    fun <- case funs of
        [fun] -> pure fun
        []    -> failure "AppBox - No funs"
        _     -> failure "AppBox - Many funs (?)"
    arg <- case args of
        [arg] -> pure arg
        []    -> failure "AppBox - No args"
        _     -> failure "AppBox - Many args (?)"
    pure $ AppBox { _boxFun = fun, _boxArg = arg, _boxOut = outs }
  where
    failure = Left . pure . LBError pos
    funs = mapMaybe isAppFun conns
    args = mapMaybe isAppArg conns
    outs = mapMaybe isAppOut conns

mkAbsBox pos conns = do
   ret <- case rets of
     [ret] -> pure ret
     _     -> failure $ "AbsBox - Number of return wires " ++ show (length rets) ++ " /= 1"
   pure $ AbsBox { _boxVar = vars, _boxRet = ret, _boxOut = outs }   
   where
    failure = Left . pure . LBError pos
    vars = mapMaybe isAbsVar conns
    rets = mapMaybe isAbsRet conns
    outs = mapMaybe isAbsOut conns

toWire :: CCWire -> Either [LBError] Wire
toWire dat
  | _hasLoop dat = Left . pure $ LBError pos "Looped Wire"
  | otherwise = Right $ _lines dat
  where
    pos = LBPosLines "" $ _lines dat

checkNoIntersectingBoxes :: Map CCID CCBox -> Validation [LBError] ()
checkNoIntersectingBoxes boxes = case inters of
  [] -> pure ()
  _  -> Failure $ map interErr inters
  where
    rects = _bb <$> boxes
    inters = intersectingBoxes rects
    interErr (i,j) = LBError (LBPosBox "" iBox) $ "Intersecting boxes:" ++ format jBox
      where
        iBox = rects ! i
        jBox = rects ! j

errUnique, errMultiOutgoing :: LBError
errUnique
  = LBError (LBPosStr "TopLevel") "There must be either a box containing all or an outgoing wire"
errMultiOutgoing
  = LBError (LBPosStr "TopLevel") "Multiple outgoing wires"

uniqueOutput :: Diagram -> Validation [LBError] CCID
uniqueOutput diag = case _diagramOutWires diag of
  [] -> case rootBoxes (_bb <$> _diagramBoxes diag) of
    [out] -> pure out
    _ -> Failure [errUnique]
  [out] -> pure out
  _ -> Failure [errMultiOutgoing]
