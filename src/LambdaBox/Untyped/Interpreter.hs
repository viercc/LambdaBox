{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE LambdaCase #-}
module LambdaBox.Untyped.Interpreter
  ( -- * Types
    DataConstructor(..),
    LBData,
    LBData',
    wrap,

    -- * evaluator
    runMain,
    withNoBuffering,
    eval,

    -- * Encode/Decode
    encodeInt,
    decodeInt,

    encodeBool,
    decodeBool,

    encodeCons,
    decodeCons,

    StreamIO(..),
    getContentsOfStreamIO,
    encodeFromStreamIO,
    decodeToStreamIO,

    encodeFromByteStream,
    decodeToByteStream,

    encodeFromList,
    encodeFromListIO,
    decodeToList,
    decodeToListIO,

    encodeStringAsByteStream,
    decodeByteStreamAsString,

    -- * Program
    ProgramTree(..),
    ProgramF(..),
    decodeProgramTree,
    runProgramTree
  )
where

import Data.Char (ord)
import Data.List (intercalate, uncons)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString.Lazy.UTF8 as BSL.UTF8

import System.IO
import qualified System.IO.Unsafe as Unsafe
import System.Exit (ExitCode(..))

import Control.Monad.Laziness
import qualified Data.Lambda.LazyEval as Lam.Eval
import Data.Lambda.LazyEval (WHNF(..), Value(..), forceValue)

import LambdaBox.Error
import LambdaBox.Untyped.Expr
import LambdaBox.Literal
import Text.PrettyPrint.Formattable
import Text.PrettyPrint.Util
import Control.Exception (bracket)
import Data.Word (Word8)
import Data.Bits

----------------------------------------------
-- Main Logic
----------------------------------------------
runMain :: LBData -> IO ExitCode
runMain main =
  do
    let input = wrap $ encodeFromStreamIO (wrap . encodeInt) readByteByByte
    mainOutput <- main `app` input
    let output = decodeToStreamIO decodeInt (wrap mainOutput)
    ret <- writeByteByByte output
    pure $ case ret - 256 of
      0 -> ExitSuccess
      err -> ExitFailure err

-- Why should I use (read|write)ByteByByte rather than
-- using lazy IO combined with lazy ByteString?
-- 
-- That's because the chunking behavior of lazy ByteString
-- are controlled independenly of buffering behavior of
-- stdin/stdout file handle. It works like another layer of
-- buffering added to stdin/stdout. Such an extra buffering
-- interacts badly when the interpreter is run with console IO.
readByteByByte :: StreamIO Int
readByteByByte = StreamIO True step
  where
    step False = pure (256 :: Int, False)
    step True = do
      bite <- BS.hGet stdin 1
      case BS.uncons bite of
        Just (b, _) -> pure (fromIntegral b, True)
        Nothing -> pure (256, False)

writeByteByByte :: StreamIO Int -> IO Int
writeByteByByte (StreamIO stream step) = loop stream
  where
    loop s = do
      (b, s') <- step s
      if 0 <= b && b < 256
        then BS.putStr (BS.singleton (fromIntegral b)) >> loop s'
        else pure b

withNoBuffering :: IO a -> IO a
withNoBuffering action =
  bracket currentModes setBackModes (const (disableBuffering >> action))
  where
    currentModes = (,) <$> hGetBuffering stdin <*> hGetBuffering stdout
    setBackModes (stdinMode, stdoutMode) = hSetBuffering stdin stdinMode >> hSetBuffering stdin stdoutMode
    disableBuffering = hSetBuffering stdin NoBuffering
      >> hSetBuffering stdout NoBuffering

----------------------------------------------
-- Evaluation
----------------------------------------------
eval :: LBExpr -> IO LBData
eval = Lam.Eval.eval noPosition (pure ()) evalLit undefinedVarErr
  where
    evalLit :: Literal -> LBData'
    evalLit (IntLit n) = encodeInt n
    evalLit (BoolLit b) = encodeBool b
    evalLit (CharLit c) = encodeInt (ord c)
    evalLit (InputStrLit str) = encodeStringAsByteStream str
    evalLit (ListLit lits) = encodeFromList (wrap . evalLit) lits

    undefinedVarErr :: VarName -> LBData'
    undefinedVarErr x = error $ "undefined variable " ++ format x

-- * LBData type definitions

data DataConstructor
  = LBInt !Int
  | LBCons
  | LBNil
  | LBRead
  | LBWrite !Bool
  deriving Show

instance Formattable DataConstructor where
  formatPrec _ (LBInt i) = formatDoc i
  formatPrec _ LBCons = pretty "cons"
  formatPrec _ LBNil = pretty "nil"
  formatPrec _ LBRead = pretty "r"
  formatPrec _ (LBWrite b) = pretty (if b then "w1" else "w0")

type LBData' = Lam.Eval.WHNF LBPosition IO DataConstructor
type LBData = Lam.Eval.Value LBPosition IO DataConstructor

wrap :: LBData' -> LBData
wrap = Lam.Eval.produceValue

-- * Low-level LBData operations

defun :: String -> (LBData -> IO LBData') -> LBData'
defun name = LamHead' (LBPosStr name)

lam :: (LBData -> IO LBData') -> LBData'
lam = LamHead' noPosition

app :: LBData -> LBData -> IO LBData'
app f x = forceValue f >>= \f' -> app' f' x

app' :: LBData' -> LBData -> IO LBData'
app' = Lam.Eval.apply'

getInt :: LBData' -> IO Int
getInt val = case val of
  ConHead' (LBInt i) [] -> pure i
  ConHead' (LBInt _) _ -> fail $ "Int with arguments? " ++ formatLBData' val
  _ -> fail $ "Not an Int? " ++ formatLBData' val

getPair :: LBData' -> IO (LBData, LBData)
getPair val = case val of
  ConHead' LBCons [y,x] -> pure (x,y)
  ConHead' LBCons _ -> fail $ "Cons with wrong number of arguments? " ++ formatLBData' val
  _ -> fail $ "Not a Cons? " ++ formatLBData' val

getUncons :: LBData' -> IO (Maybe (LBData, LBData))
getUncons val = case val of
  ConHead' LBNil [] -> pure Nothing
  ConHead' LBNil _ -> fail $ "Nil with arguments? " ++ formatLBData' val
  ConHead' LBCons [y,x] -> pure (Just (x,y))
  ConHead' LBCons _ -> fail $ "Cons with wrong number of arguments? " ++ formatLBData' val
  _ -> fail $ "Not a Nil nor a Cons? " ++ formatLBData' val

formatLBData' :: LBData' -> String
formatLBData' = formatLBData . wrap

formatLBData :: LBData -> String
formatLBData (ConHead con []) = format con
formatLBData (ConHead con args) = format con ++ "(" ++ intercalate "," (formatLBData <$> args) ++ ")"
formatLBData (LamHead ann _) = "Lam<" ++ format ann ++ ">"
formatLBData (ThunkValue ann _) = "thunk<" ++ format ann ++ ">"

---- * Encode / Decode

encodeInt :: Int -> LBData'
encodeInt = \n -> lam $ \f -> pure (lam $ \x -> encodeInt' n f x)
  where
    encodeInt' n f x
      | n <= 0 = forceValue x
      | otherwise = do
          f' <- forceValue f
          arg <- ThunkValue noPosition <$> delay (encodeInt' (n - 1) f x)
          app' f' arg

decodeInt :: LBData -> IO Int
decodeInt n = do
  ns <- app n succ'
  nsz <- app' ns zero'
  getInt nsz
  where
    pos = LBPosStr "decodeInt"

    zero' = ConHead (LBInt 0) []
    succ' = LamHead pos $ \x -> do
      x' <- forceValue x
      i <- getInt x'
      let !i' = LBInt (i + 1)
      pure (ConHead' i' [])

encodeBool :: Bool -> LBData'
encodeBool b = if b then true' else false'
  where
    true', false' :: LBData'
    true' = defun "encode/true" $ \x -> pure $ lam $ \_ -> forceValue x
    false' = defun "encode/false" $ \_ -> pure $ lam $ \y -> forceValue y

decodeBool :: LBData -> IO Bool
decodeBool x = do
  xT <- app x (ConHead (LBInt 1) [])
  xTF <- app' xT (ConHead (LBInt 0) [])
  i <- getInt xTF
  pure (i == 1)

encodeCons :: LBData -> LBData -> LBData'
encodeCons a b = defun "encode/pair" $ \f -> do
  f' <- forceValue f
  fa <- Lam.Eval.apply' f' a
  Lam.Eval.apply' fa b

decodeCons :: LBData -> IO (LBData, LBData)
decodeCons p = do
  pCons <- app p (ConHead LBCons [])
  getPair pCons

data StreamIO a = forall s. StreamIO s (s -> IO (a, s))

encodeFromStreamIO :: (a -> LBData) -> StreamIO a -> LBData'
encodeFromStreamIO encodeElem (StreamIO s0 unconsIO) = go s0
  where
    go s = defun "encode/stream" $ \f -> do
      (a,s') <- unconsIO s
      fa <- app f (encodeElem a)
      app' fa (wrap $ go s')

decodeToStreamIO :: (LBData -> IO a) -> LBData -> StreamIO a
decodeToStreamIO decodeElem stream = StreamIO stream step
  where
    step s = do
      (aData,s') <- decodeCons s
      a <- decodeElem aData
      pure (a, s')

encodeFromByteStream :: BSL.ByteString -> LBData'
encodeFromByteStream bs = encodeFromStreamIO id $ StreamIO (BSL.unpack bs) (pure . step)
  where
    step [] = (nat256, [])
    step (x : xs) = (wrap (encodeInt (fromIntegral x)), xs)

    nat256 = wrap $ encodeInt 256

decodeToByteStream :: LBData -> IO (BSL.ByteString, Int)
decodeToByteStream stream = do
  xs <- getContentsOfStreamIO (decodeToStreamIO decodeInt stream)
  let (content, rest) = span (< 256) xs
  pure (BSL.pack (fromIntegral <$> content), head rest)

encodeFromList :: (a -> LBData) -> [a] -> LBData'
encodeFromList f as = encodeFromListIO f as (pure . unconsList)
  where
    unconsList = Data.List.uncons

encodeFromListIO :: (a -> LBData) -> s -> (s -> IO (Maybe (a,s))) -> LBData'
encodeFromListIO f stream unconsIO = go stream
  where
    go s = defun "encode/list" $ \c ->
      pure $ lam $ \n -> do
        sHead <- unconsIO s
        case sHead of
          Nothing -> forceValue n
          Just (a, s') -> do
            ca <- app c (f a)
            app' ca (wrap $ go s')

decodeToList :: (LBData -> IO a) -> LBData -> IO [a]
decodeToList f = unsafeInterleavedUnfoldrIO (decodeToListIO f)

getContentsOfStreamIO :: StreamIO a -> IO [a]
getContentsOfStreamIO (StreamIO s step) =
  unsafeInterleavedUnfoldrIO (fmap Just . step) s

unsafeInterleavedUnfoldrIO :: (s -> IO (Maybe (a, s))) -> s -> IO [a]
unsafeInterleavedUnfoldrIO step = go
  where
    go s = Unsafe.unsafeInterleaveIO $ do
      sHead <- step s
      case sHead of
        Nothing -> pure []
        Just (a,s') -> (a :) <$> go s'

decodeToListIO :: (LBData -> IO a) -> LBData -> IO (Maybe (a, LBData))
decodeToListIO f xs = do
  xsCons <- app xs (ConHead LBCons [])
  xsConsNil <- app' xsCons (ConHead LBNil [])
  xsUnconsed <- getUncons xsConsNil
  case xsUnconsed of
    Nothing -> pure Nothing
    Just (aData, xs') -> f aData >>= \a -> pure (Just (a, xs'))

encodeStringAsByteStream :: String -> LBData'
encodeStringAsByteStream = encodeFromByteStream . BSL.UTF8.fromString

decodeByteStreamAsString :: LBData -> IO String
decodeByteStreamAsString = fmap (BSL.UTF8.toString . fst) . decodeToByteStream

-- * Program

data ProgramF r = ProgramRead r r r | ProgramWrite !Bool r | ProgramHalt
  deriving (Show, Eq, Ord, Functor)

newtype ProgramTree = ProgramTree { getProgramInstruction :: IO (ProgramF ProgramTree) }

decodeProgramTree :: LBData -> ProgramTree
decodeProgramTree programDef = ProgramTree $ do
  xR <- app programDef (ConHead LBRead [])
  xR0 <- app' xR (ConHead (LBWrite False) [])
  xR01 <- app' xR0 (ConHead (LBWrite True) [])
  xR01h <- app' xR01 (ConHead LBNil [])
  getF xR01h
  where
    getF x' = case x' of
      ConHead' LBRead [keof, k1, k0] -> pure $ ProgramRead (toTree k0) (toTree k1) (toTree keof)
      ConHead' LBRead _ -> fail $ "Wrong arity " ++ formatLBData' x'  
      ConHead' (LBWrite b) [k] -> pure $ ProgramWrite b (toTree k)
      ConHead' (LBWrite _) _ -> fail $ "Wrong arity " ++ formatLBData' x'
      ConHead' LBNil [] -> pure ProgramHalt
      ConHead' LBNil _ -> fail $ "Wrong arity " ++ formatLBData' x'
      _ -> fail $ "Not a program node?" ++ formatLBData' x'
    toTree x = ProgramTree $ forceValue x >>= getF

runProgramTree :: ProgramTree -> IO ExitCode
runProgramTree = go emptyInBuf emptyOutBuf
  where
    go :: InBuf -> OutBuf -> ProgramTree -> IO ExitCode
    go inBuf outBuf tree = do
      node <- getProgramInstruction tree
      case node of
        ProgramRead k0 k1 keof ->
          readBit inBuf >>= \case
            Nothing              -> goEOF outBuf keof
            Just (inBuf', False) -> go inBuf' outBuf k0
            Just (inBuf', True)  -> go inBuf' outBuf k1 
        ProgramWrite b k -> do
          outBuf' <- writeBit outBuf b
          go inBuf outBuf' k
        ProgramHalt -> pure (outToExitStatus outBuf)
    
    goEOF :: OutBuf -> ProgramTree -> IO ExitCode
    goEOF !outBuf tree = do
      node <- getProgramInstruction tree
      case node of
        ProgramRead _ _ keof -> goEOF outBuf keof
        ProgramWrite b k -> do
          outBits' <- writeBit outBuf b
          goEOF outBits' k
        ProgramHalt -> pure (outToExitStatus outBuf)
    
    outToExitStatus (OutBuf n outWord)
      | n == 0        = ExitSuccess
      | otherwise     = ExitFailure (fromIntegral (setBit outWord n - 1))

data InBuf = InBuf !Int !Word8

emptyInBuf :: InBuf
emptyInBuf = InBuf 0 0

readBit :: InBuf -> IO (Maybe (InBuf, Bool))
readBit (InBuf inBits inWord)
  | inBits == 0 = do
      inputStr <- BS.hGet stdin 1
      pure $ case BS.uncons inputStr of
        Just (inWord', _) -> Just (InBuf 1 inWord', testBit inWord' 0)
        Nothing -> Nothing
  | otherwise =
      let inBits' = (inBits + 1) `mod` 8
      in pure $ Just (InBuf inBits' inWord, testBit inWord inBits)

data OutBuf = OutBuf !Int !Word8

emptyOutBuf :: OutBuf
emptyOutBuf = OutBuf 0 0

writeBit :: OutBuf -> Bool -> IO OutBuf
writeBit (OutBuf n outWord) b
  | isFull = do BS.hPutStr stdout (BS.singleton outWord')
                pure emptyOutBuf
  | otherwise = pure (OutBuf n' outWord')
  where
    n' = n + 1
    outWord' = if b then setBit outWord n else outWord
    isFull = n' == 8