module LambdaBox.Error where

import Data.List (intersperse)
import Geom.Line
import Geom.Rect
import Text.PrettyPrint.Formattable
import Text.PrettyPrint.Util

data LBError = LBError !LBPosition !ErrorMes
  deriving (Show, Eq)

type Path = String

data LBPosition
  = LBPosBox !Path !Rect
  | LBPosLines !Path ![Line]
  | LBPosStr !String
  deriving (Show, Eq)

type ErrorMes = String

noPosition :: LBPosition
noPosition = LBPosStr ""

posGetPath :: LBPosition -> Path
posGetPath (LBPosBox path _) = path
posGetPath (LBPosLines path _) = path
posGetPath (LBPosStr _) = "*builtin*"

posSetPath :: Path -> LBPosition -> LBPosition
posSetPath path (LBPosBox _ rect) = LBPosBox path rect
posSetPath path (LBPosLines _ ls) = LBPosLines path ls
posSetPath _ (LBPosStr str) = LBPosStr str

instance Formattable LBPosition where
  formatPrec _ (LBPosBox path b) = pretty path <> pretty ":" <> formatDoc b
  formatPrec _ (LBPosLines path ls) =
    let (hd, tl) = splitAt 3 ls
        content = hcat . intersperse (pretty ", ") $ map formatDoc hd
        remain = if null tl then mempty else pretty ", ..."
     in pretty path <> pretty ":[" <> content <> remain <> pretty "]"
  formatPrec _ (LBPosStr str) = pretty "*builtin*:" <> pretty str

instance Formattable LBError where
  formatPrec _ (LBError pos mes) =
    pretty "at" <+> formatDoc pos <> hardline <> nest 4 (pretty mes)
