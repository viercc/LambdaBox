module Data.Relation(
    Rel,
    lookupL,
    fromList,
    difference,
    compose,
    range
) where

import Data.Foldable (fold)
import Data.Map.Strict (Map)
import Data.Set (Set, (\\))
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

-- * Relation utilities

type Rel a b = Map a (Set b)

lookupL :: (Ord a) => a -> Rel a b -> Set b
lookupL = Map.findWithDefault Set.empty

fromList :: (Ord a, Ord b) => [(a, b)] -> Rel a b
fromList kvs = Map.fromListWith Set.union [(a, Set.singleton b) | (a, b) <- kvs]

difference :: (Ord a, Ord b) => Rel a b -> Rel a b -> Rel a b
difference = Map.differenceWith (\bs bs' -> nonEmptySet (bs \\ bs'))

compose :: (Ord a, Ord b, Ord c) => Rel a b -> Rel b c -> Rel a c
compose ab bc = Map.mapMaybe (nonEmptySet . foldMap (\b -> lookupL b bc)) ab

nonEmptySet :: Set a -> Maybe (Set a)
nonEmptySet s = if Set.null s then Nothing else Just s

range :: (Ord b) => Rel a b -> Set b
range = Data.Foldable.fold