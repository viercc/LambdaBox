module Data.PointSet(
    PointSet(),
    fromList, toList,
    toMapOfSet,
    fromMapOfSet,

    transpose,
    union,
    intersection,
    difference
) where

import Geom (Point(..))

import qualified Data.IntSet as IS
import qualified Data.IntMap as IM
import Data.Functor.Classes (showsUnaryWith)

newtype PointSet = MkPointSet (IM.IntMap IS.IntSet)
    deriving (Eq, Ord)

instance Show PointSet where
    showsPrec p points = showsUnaryWith showsPrec "fromList" p (toList points)

fromList :: [Point] -> PointSet
fromList ps = MkPointSet $ IM.fromListWith IS.union [ (i, IS.singleton j) | Point i j <- ps ]

toList :: PointSet -> [Point]
toList (MkPointSet m) =
    [ Point i j | (i, js) <- IM.toAscList m
                , j <- IS.toAscList js ]

toMapOfSet :: PointSet -> IM.IntMap IS.IntSet
toMapOfSet (MkPointSet m) = m

fromMapOfSet :: IM.IntMap IS.IntSet -> PointSet
fromMapOfSet = MkPointSet . IM.filter (not . IS.null)

transpose :: PointSet -> PointSet
transpose (MkPointSet m) = MkPointSet $
    IM.unionsWith IS.union
      [ IM.fromSet (const (IS.singleton i)) js | (i, js) <- IM.toAscList m ]

union :: PointSet -> PointSet -> PointSet
union (MkPointSet m1) (MkPointSet m2) = MkPointSet $
  IM.unionWith IS.union m1 m2

intersection :: PointSet -> PointSet -> PointSet
intersection (MkPointSet m1) (MkPointSet m2) = MkPointSet $
    IM.mapMaybe nonEmpty $ IM.intersectionWith IS.intersection m1 m2

difference :: PointSet -> PointSet -> PointSet
difference (MkPointSet m1) (MkPointSet m2) = MkPointSet $
    IM.differenceWith (\s1 s2 -> nonEmpty $ IS.difference s1 s2) m1 m2

nonEmpty :: IS.IntSet -> Maybe IS.IntSet
nonEmpty s = if IS.null s then Nothing else Just s
