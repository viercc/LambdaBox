module Data.IntervalSet(
    IntervalSet(),
    null,
    size,

    minView,
    member,
    queryAt,
    splitQueryAt,

    empty,
    singleton,
    insert,
    union,
    restrict,
    intersection,
    filter, 
    deleteExact,
    partition,

    fromIntervals, toIntervals,
    fromIntSet, toIntSet,

    fromBools,

    shift
) where

import Prelude hiding (filter, null)
import qualified Data.List as List
import qualified Data.IntSet as IS

import Data.Functor.Classes (showsUnaryWith)

-- | Representation of arbitrary set of @Int@s, as a union of disjoint intervals.
newtype IntervalSet = MkIntervalSet IS.IntSet
    deriving (Eq, Ord)

null :: IntervalSet -> Bool
null (MkIntervalSet xs) = IS.null xs

-- | Size of @s :: IntervalSet@ is the number of intervals,
--   *not* the number of @Int@ values satisfying @`'member'` s@.
size :: IntervalSet -> Int
size (MkIntervalSet xs) = IS.size xs `div` 2

splitIntSetLE :: Int -> IS.IntSet -> (IS.IntSet, IS.IntSet)
splitIntSetLE x xs = case IS.splitMember x xs of
    (lefts, True, rights) -> (IS.insert x lefts, rights)
    (lefts, False, rights) -> (lefts, rights)

errOddIntSet :: Int -> a
errOddIntSet n = error $ "Must have even number of elements but: size xs == " ++ show n

minView :: IntervalSet -> Maybe ((Int, Int), IntervalSet)
minView (MkIntervalSet xs) = do
    (x0, xs') <- IS.minView xs
    (x1, xs'') <- IS.minView xs'
    Just ((x0, x1), MkIntervalSet xs'')

member :: Int -> IntervalSet -> Bool
member x (MkIntervalSet xs) = odd $ IS.size (IS.dropWhileAntitone (<= x) xs)

queryAt :: Int -> IntervalSet -> Maybe (Int, Int)
queryAt x s = case splitQueryAt x s of
    (_, ans, _) -> ans

splitQueryAt :: Int -> IntervalSet -> (IntervalSet, Maybe (Int, Int), IntervalSet)
splitQueryAt x (MkIntervalSet xs) = case splitIntSetLE x xs of
    (lefts, rights)
      | odd (IS.size lefts) && odd (IS.size rights) ->
          case (IS.deleteFindMax lefts, IS.deleteFindMin rights) of
            ((x0, lefts'), (x1, rights')) -> (MkIntervalSet lefts', Just (x0, x1), MkIntervalSet rights')
      | even (IS.size lefts) && even (IS.size rights) -> (MkIntervalSet lefts, Nothing, MkIntervalSet rights)
      | otherwise -> errOddIntSet (IS.size xs)

empty :: IntervalSet
empty = MkIntervalSet IS.empty

singleton :: (Int,Int) -> IntervalSet
singleton (x0, x1)
  | x0 < x1 = MkIntervalSet $ IS.fromDistinctAscList [x0, x1]
  | otherwise = empty

splitUnionInterval :: (Int, Int) -> IntervalSet -> (IntervalSet, (Int, Int), IntervalSet)
splitUnionInterval r@(x0, x1) rs = case splitQueryAt (pred x0) rs of
    (lefts, leftHit, midRights) -> case splitQueryAt x1 midRights of
        (_, rightHit, rights) -> (lefts, r `unionM` leftHit `unionM` rightHit, rights)
    where
        unionM s = maybe s (unionInterval s)

unionInterval :: (Int,Int) -> (Int,Int) -> (Int,Int)
unionInterval (x0,x1) (y0,y1) = (min x0 y0, max x1 y1)

insert :: (Int, Int) -> IntervalSet -> IntervalSet
insert r rs = case splitUnionInterval r rs of
    (lefts, mid, rights) -> lefts `merge` singleton mid `merge` rights
    where
        merge = unsafeDisjointUnion

splitLT :: Int -> IntervalSet -> (IntervalSet, IntervalSet)
splitLT x rs = case splitQueryAt x rs of
    (lefts, Nothing, rights) -> (lefts, rights)
    (lefts, Just (x0, x1), rights) -> (lefts `merge` singleton (x0, x), rights `merge` singleton (x, x1))
    where
       merge = unsafeDisjointUnion

unsafeDisjointUnion :: IntervalSet -> IntervalSet -> IntervalSet
unsafeDisjointUnion (MkIntervalSet as) (MkIntervalSet bs) = MkIntervalSet (as `IS.union` bs)

unsafeDisjointUnions :: [IntervalSet] -> IntervalSet
unsafeDisjointUnions = List.foldl' unsafeDisjointUnion empty

union :: IntervalSet -> IntervalSet -> IntervalSet
union rs1 rs2 = unsafeDisjointUnions $ aux rs1 rs2
  where
    aux s t | size s <= size t = step s t
            | otherwise = step t s
    
    step s t = case minView s of
        Nothing -> [t]
        Just (r, s') -> loop r s' t
    loop r s t = case splitUnionInterval r t of
        (lefts, r', rights)
          | r == r'   -> lefts : singleton r' : aux s rights
          | otherwise -> lefts : loop r' rights s

restrict :: (Int, Int) -> IntervalSet -> IntervalSet
restrict (x0, x1) = fst . splitLT x1 . snd . splitLT x0

intersection :: IntervalSet -> IntervalSet -> IntervalSet
intersection rs1 rs2 = unsafeDisjointUnions result
  where
    result | size rs1 <= size rs2 = aux (toIntervals rs1) rs2
           | otherwise            = aux (toIntervals rs2) rs1
    aux [] _ = []
    aux ((x0, x1) : rs) t = case splitLT x1 t of
        (midLeft, right) -> snd (splitLT x0 midLeft) : aux rs right

filter :: ((Int, Int) -> Bool) -> IntervalSet -> IntervalSet
filter p (MkIntervalSet xs) = unsafeFromList . List.filter p . pairs $ IS.toAscList xs

-- @deleteExact interval == filter (/= interval)@
deleteExact :: (Int, Int) -> IntervalSet -> IntervalSet
deleteExact interval s = case splitQueryAt (fst interval) s of
  (lefts, Just interval', rights)
    | interval == interval' -> unsafeDisjointUnion lefts rights
  _ -> s

partition :: ((Int,Int) -> Bool) -> IntervalSet -> (IntervalSet, IntervalSet)
partition p (MkIntervalSet xs) = (unsafeFromList ys, unsafeFromList zs)
  where
    (ys, zs) = List.partition p . pairs $ IS.toAscList xs

pairs :: [a] -> [(a,a)]
pairs [] = []
pairs [_] = []
pairs (x0:x1:xs) = (x0,x1) : pairs xs

--------------

unsafeFromList :: [(Int, Int)] -> IntervalSet
unsafeFromList = MkIntervalSet . IS.fromDistinctAscList . unpairs
  where
    unpairs :: [(a,a)] -> [a]
    unpairs = concatMap (\(x0,x1) -> [x0,x1])

fromIntervals :: [(Int, Int)] -> IntervalSet
fromIntervals = List.foldl' (flip insert) empty

toIntervals :: IntervalSet -> [(Int, Int)]
toIntervals (MkIntervalSet xs)
  | even (IS.size xs) = pairs (IS.toAscList xs)
  | otherwise         = errOddIntSet (IS.size xs)

runs :: [Int] -> [(Int, Int)]
runs = runs'
  where
    runs' [] = []
    runs' (x:xs) = go (x, succ x) xs
    
    go (x0, x1) (next : rest)
      | x1 == next = go (x0, succ next) rest
    go r xs = r : runs' xs

fromIntSet :: IS.IntSet -> IntervalSet
fromIntSet = unsafeFromList . runs . IS.toAscList

toIntSet :: IntervalSet -> IS.IntSet
toIntSet s = IS.fromDistinctAscList $ toIntervals s >>= \(x0, x1) -> [x0 .. pred x1]

fromBools :: [Bool] -> IntervalSet
fromBools bs = unsafeFromList $ runs . map fst . List.filter snd $ zip [0..] bs

shift :: Int -> IntervalSet -> IntervalSet
shift k (MkIntervalSet xs) = MkIntervalSet $ IS.mapMonotonic (k +) xs

instance Show IntervalSet where
    showsPrec p s = showsUnaryWith showsPrec "fromIntervals" p (toIntervals s)