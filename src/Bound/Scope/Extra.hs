{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Bound.Scope.Extra(
    Scope',

    Alg, scopeAlg, scopeAlg',

    Binder, scopeBinder,

    bimapScope, firstScope, bifoldMapScope, bitraverseScope,
    liftEq2Scope,

    Var(..), unvar, unvar', maskEq,
    
    countBound, abstractMany
) where

import Data.Bifunctor
import Data.Bifoldable

import Bound.Var
import Bound.Scope
import Data.Monoid (Sum(..))
import qualified Data.Map.Strict as Map

import Text.PrettyPrint.Formattable
import Prettyprinter (Doc, pretty)
import Data.Functor.Classes

type Scope' = Scope ()

-- Algebra

type Alg f b = forall a. (a -> b) -> f a -> b

scopeAlg :: (c -> b) -> Alg f b -> Alg (Scope c f) b
scopeAlg k alg f = alg (unvar k (alg f)) . unscope

-- scopeAlg' b alg == \f ma -> alg id (instantiate' (pure b) (f <$> ma)) 
scopeAlg' :: b -> Alg f b -> Alg (Scope' f) b
scopeAlg' b = scopeAlg (const b)

-- Binder

type Binder f g = forall a b. (a -> g b) -> f a -> g b

scopeBinder :: Monad g => Binder f g -> (a -> g b) -> Scope c f a -> Scope c g b
scopeBinder binder f (Scope t) = Scope $ binder (unvar (pure . B) (binder (pure . F . f))) t

-- Scope on bifunctors

bimapScope :: (Bifunctor f) => (a -> a') -> (b -> b') -> Scope c (f a) b -> Scope c (f a') b'
bimapScope f g = Scope . bimap f (fmap (bimap f g)) . unscope

firstScope :: (Bifunctor f) => (a -> a') -> Scope c (f a) b -> Scope c (f a') b
firstScope f = hoistScope (first f)

bifoldMapScope :: (Bifoldable f, Monoid m) => (a -> m) -> (b -> m) -> Scope c (f a) b -> m
bifoldMapScope f g = bifoldMap f (foldMap (bifoldMap f g)) . unscope

-- This is **in**exact equality!
liftEq2Scope :: (Eq c, Eq2 f, Monad (f a), Monad (f a')) => (a -> a' -> Bool) -> (b -> b' -> Bool) -> Scope c (f a) b -> Scope c (f a') b' -> Bool
liftEq2Scope eqA eqB t t' = liftEq2 eqA (liftEq eqB) (fromScope t) (fromScope t')

-- Misc.

unvar' :: b -> (a -> b) -> Var () a -> b
unvar' b = unvar (const b)

maskEq :: (Eq a) => a -> a -> Var () a
maskEq a a' = if a == a' then B () else F a'

countBound :: Foldable f => Scope c f a -> Int
countBound = getSum . foldMap (unvar (const 1) (const 0)) . unscope

abstractMany :: (Monad f, Ord a) => [a] -> f a -> Scope Int f a
abstractMany xs = abstractEither (\a -> maybe (Right a) Left $ Map.lookup a table)
  where
    table = Map.fromList [ (x,i) | (i,x) <- zip [0..] xs ]

-- orphan instances

class BoundVarFormat a where
    bvFormat :: a -> Doc ann

instance BoundVarFormat () where
    bvFormat _ = "0"

instance BoundVarFormat Int where
    bvFormat = pretty

instance (BoundVarFormat c, Formattable a) => Formattable (Var c a) where
    formatPrec _ (B c) = bvFormat c
    formatPrec _ (F a) = formatPrec 11 a <> "ʹ"
      -- prime symbol (U+02B9), not the single quote ['] (U+0027)

instance (Formattable (f (Var c (f a)))) => Formattable (Scope c f a) where
    formatPrec p (Scope t) = formatPrec p t