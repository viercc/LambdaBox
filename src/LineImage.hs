module LineImage
  ( LineImage (),
    fromPointSet,
    toPointSet,
    intersections,
    intersectionsWithRel,
    getLines,
    getHorizontalLines,
    getVerticalLines,
    lookupLinesAt,

    member,
    insertLine,
    deleteLine
  )
where

-- import Debug.Trace

import qualified Data.IntMap.Lazy as IM
import Data.IntervalSet (IntervalSet)
import qualified Data.IntervalSet as Ival
import Data.Maybe (fromMaybe, mapMaybe, maybeToList)
import Data.PointSet as PS
import Data.Tuple (swap)
import Geom
import Geom.Line
import qualified Text.PrettyPrint.Formattable as PP
import qualified Text.PrettyPrint.Util as PP

--------------------------------------------------------
--  Line Set Representation of Image
--------------------------------------------------------
data LineImage = LineImage
  { _horizontal :: IM.IntMap IntervalSet,
    _vertical :: IM.IntMap IntervalSet,
    _isolated :: PS.PointSet
  }
  deriving (Show, Eq, Ord)

pointsToScans :: PS.PointSet -> IM.IntMap IntervalSet
pointsToScans = IM.mapMaybe (nonEmptyIntervalSet . Ival.filter isLong . Ival.fromIntSet) . PS.toMapOfSet
  where
    isLong (x0, x1) = x1 - x0 >= 2

scansToPoints :: IM.IntMap IntervalSet -> PS.PointSet
scansToPoints = PS.fromMapOfSet . IM.map Ival.toIntSet

fromPointSet :: PS.PointSet -> LineImage
fromPointSet points = LineImage hLines vLines singles
  where
    vPoints = points 
    hPoints = PS.transpose points

    vLines = pointsToScans vPoints
    hLines = pointsToScans hPoints
    singles = (vPoints `PS.difference` scansToPoints vLines) `PS.difference` PS.transpose (scansToPoints hLines)

toPointSet :: LineImage -> PS.PointSet
toPointSet (LineImage hLines vLines singles) = hPoints `PS.union` vPoints `PS.union` singles
  where
    hPoints = PS.transpose $ PS.fromMapOfSet $ IM.map Ival.toIntSet hLines
    vPoints = PS.fromMapOfSet $ IM.map Ival.toIntSet vLines

queryLines :: Int -> Range -> IM.IntMap IntervalSet -> [(Int, Range)]
queryLines x (Range y1 y2) = mapMaybe (\(y, xs) -> (,) y . ivalToRange <$> Ival.queryAt x xs) . IM.toAscList . takeBetween y1 y2

ivalToRange :: (Int, Int) -> Range
ivalToRange (x0, x1) = Range x0 (pred x1)

intersections :: Line -> LineImage -> [Line]
intersections line image = case line of
  Vertical x ys -> map (uncurry Horizontal . swap) $ queryLines x ys (_horizontal image)
  Horizontal xs y -> map (uncurry Vertical) $ queryLines y xs (_vertical image)

intersectionsWithRel :: Line -> LineImage -> [(LineRelation, Line)]
intersectionsWithRel line image = (\line' -> (noDisjointCase (lineRelation line line'), line')) <$> intersections line image
  where
    noDisjointCase = fromMaybe (error "The result must be Just")

-- `takeBetween from to map` takes submap of `map` consists of keys
-- that satisfies `from` <= key <= `to`
takeBetween :: Int -> Int -> IM.IntMap a -> IM.IntMap a
takeBetween from to = takeGE from . takeLE to
  where
    takeGE key = IM.dropWhileAntitone (< key)
    takeLE key = IM.takeWhileAntitone (<= key)

mapToRanges :: IM.IntMap IntervalSet -> [(Int, Range)]
mapToRanges m =
  [ (y, ivalToRange xIval)
    | (y, xs) <- IM.toAscList m,
      xIval <- Ival.toIntervals xs
  ]

getLines :: LineImage -> [Line]
getLines image = getHorizontalLines image ++ getVerticalLines image

getHorizontalLines :: LineImage -> [Line]
getHorizontalLines image = horizontals
  where
    horizontals = map (uncurry Horizontal . swap) . mapToRanges $ _horizontal image

getVerticalLines :: LineImage -> [Line]
getVerticalLines image = verticals
  where
    verticals = map (uncurry Vertical) . mapToRanges $ _vertical image

member :: Line -> LineImage -> Bool
member (Horizontal (Range x1 x2) y) im =
  (IM.lookup y (_horizontal im) >>= Ival.queryAt x1) == Just (x1, succ x2)
member (Vertical x (Range y1 y2)) im =
  (IM.lookup x (_vertical im) >>= Ival.queryAt y1) == Just (y1, succ y2)

insertLine :: Line -> LineImage -> LineImage
insertLine (Horizontal (Range x1 x2) y) im = im { _horizontal = hLines' }
  where
    hLines' = IM.insertWith Ival.union y (Ival.singleton (x1, succ x2)) (_horizontal im)
insertLine (Vertical x (Range y1 y2)) im = im { _vertical = vLines' }
  where
    vLines' = IM.insertWith Ival.union x (Ival.singleton (y1, succ y2)) (_vertical im)

deleteLine :: Line -> LineImage -> LineImage
deleteLine (Horizontal (Range x1 x2) y) im = im{ _horizontal = hLines' }
  where
    hLines = _horizontal im
    hLines' = IM.update upd y hLines
    upd xs = nonEmptyIntervalSet $ Ival.deleteExact (x1, succ x2) xs
deleteLine (Vertical x (Range y1 y2)) im = im { _vertical = vLines' }
  where
    vLines = _vertical im
    vLines' = IM.update upd x vLines
    upd ys = nonEmptyIntervalSet $ Ival.deleteExact (y1, succ y2) ys

nonEmptyIntervalSet :: IntervalSet -> Maybe IntervalSet
nonEmptyIntervalSet xs = if Ival.null xs then Nothing else Just xs

lookupLinesAt :: Point  -> LineImage -> [Line]
lookupLinesAt (Point x y) image = maybeToList horizontals ++ maybeToList verticals
  where
    horizontals = (`Horizontal` y) <$> query (_horizontal image) y x
    verticals = Vertical x <$> query (_vertical image) x y
    query im u v = IM.lookup u im >>= fmap ivalToRange . Ival.queryAt v

instance PP.Formattable LineImage where
  formatPrec p lineImage = PP.formatApp p (PP.pretty "LineImage") (getLines lineImage)
