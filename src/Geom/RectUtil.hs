{-# LANGUAGE BlockArguments #-}
module Geom.RectUtil where


import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as Map
import qualified Data.Set as Set
import Data.Relation (Rel)
import qualified Data.Relation as Rel

import Geom
import Geom.Rect
import Control.Monad (guard)
import Data.List (sortBy)
import Data.Ord (comparing)

directChildrenRel :: Ord k => Map k Rect -> Rel k k
directChildrenRel boxes = r `Rel.difference` (r `Rel.compose` r)
  where
    r = Rel.fromList do
      (i, iBox) <- Map.toList boxes
      (j, jBox) <- Map.toList boxes
      guard (i /= j)
      guard (shapeRel iBox jBox == StrictContains)
      pure (i,j)

intersectingBoxes :: Ord k => Map k Rect -> [(k,k)]
intersectingBoxes = go . Map.toList
  where
    go [] = []
    go ((i, iBox) : rest) =
        [ (i,j) | (j, jBox) <- rest, shapeRel iBox jBox == StrictIntersects ]
          ++ go rest

rootBoxes :: Ord k => Map k Rect -> [k]
rootBoxes boxes = Set.toList roots
  where
    nonRoots = Set.fromList do
      (i, iBox) <- Map.toList boxes
      (j, jBox) <- Map.toList boxes
      guard (i /= j)
      guard (shapeRel iBox jBox == StrictContains)
      pure j
    roots = Map.keysSet boxes Set.\\ nonRoots

maximalBoxes :: [Rect] -> [Rect]
maximalBoxes boxes = go (sortBy (comparing cornerPt) boxes)
  where
    cornerPt Rect{ rectTop = y, rectLeft = x } = (x,y)
    go [] = []
    go (x : rest) = x : go [ y | y <- rest, not (x `contains` y) ]
