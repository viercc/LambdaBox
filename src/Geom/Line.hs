module Geom.Line(
  Line(..),
  rectBorders,
  formsRectangle,

  Direction(..),
  LineRelation(..),
  lineRelation
) where

import Data.List (sort)
import Control.Monad (guard)
import Text.PrettyPrint.Formattable
import Text.PrettyPrint.Util

import Geom
import Geom.Rect

-- | Horizontal or Vertical line
data Line = Horizontal !Range !Int | Vertical !Int !Range
  deriving (Show, Read, Eq, Ord)

instance ShapeCompare Line where
  shapeRel l1 l2 = boundingBox l1 `shapeRel` boundingBox l2

rectBorders :: Rect -> [Line]
rectBorders (Rect x1 y1 x2 y2) = [leftLine, topLine, rightLine, bottomLine]
  where
    topLine = Horizontal (Range x1 x2) y1
    bottomLine = Horizontal (Range x1 x2) y2
    leftLine = Vertical x1 (Range y1 y2)
    rightLine = Vertical x2 (Range y1 y2)

formsRectangle :: [Line] -> Maybe Rect
formsRectangle ls = do
  [_, _, _, _] <- Just ls -- Check if the length is 4 before sorting
  [Horizontal xs1 y1, Horizontal xs2 y2, Vertical x1 ys1, Vertical x2 ys2] <- Just (sort ls)
  let xs = Range x1 x2
      ys = Range y1 y2
  guard $ xs1 == xs && xs2 == xs && ys1 == ys && ys2 == ys
  Just (rangesToRect xs ys)


instance BoundingBox Line where
  boundingBox (Horizontal (Range x1 x2) y) = Rect x1 y x2 y
  boundingBox (Vertical x (Range y1 y2)) = Rect x y1 x y2

instance Formattable Line where
  formatPrec p (Horizontal xs y) =
    formatApp p (formatApp 10 (pretty "Line") xs) y
  formatPrec p (Vertical x ys) =
    formatApp p (formatApp 10 (pretty "Line") x) ys

--


data Direction = North | East | South | West
  deriving (Show, Eq, Ord, Enum, Bounded)

data LineRelation
  = Cross
  | -- | Direction the line do not passes through.
    --
    -- Tee North is:
    --
    -- >    --+--
    -- >      |
    --
    -- Tee West is:
    --
    -- >  |
    -- >  +--
    -- >  |
    Tee Direction
  | -- | Direction which come first by clockwise
    -- e.g. A south-east corner
    --
    -- >   |
    -- > --+
    --
    -- ... is @Corner East@, because @East@ come first.
    Corner Direction
  deriving (Show, Eq, Ord)

instance Formattable LineRelation

lineRelation :: Line -> Line -> Maybe LineRelation
lineRelation (Horizontal xs y0) (Vertical x0 ys) = lineRelationAux y0 xs x0 ys
lineRelation v@Vertical {} h@Horizontal {} = lineRelation h v
lineRelation _ _ = Nothing

data InRange = PointLowBound | PointMid | PointHighBound
  deriving (Eq, Ord, Enum, Bounded)

inRange :: Int -> Range -> Maybe InRange
inRange p (Range lo hi)
  | p < lo || p > hi = Nothing
  | p == lo = Just PointLowBound
  | p == hi = Just PointHighBound
  | otherwise {- lo < p < hi -} = Just PointMid

lineRelationAux :: Int -> Range -> Int -> Range -> Maybe LineRelation
lineRelationAux y0 xs x0 ys = judge <$> inRange x0 xs <*> inRange y0 ys
  where
    table =
      [ [Corner West, Tee North, Corner North],
        [Tee West, Cross, Tee East],
        [Corner South, Tee South, Corner East]
      ]
    judge xpos ypos = table !! fromEnum ypos !! fromEnum xpos