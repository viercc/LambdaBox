{-# LANGUAGE InstanceSigs #-}
module Geom.Rect where


import Text.PrettyPrint.Formattable
import Text.PrettyPrint.Util

import Geom

-- | Rectangle
--
-- @Rect x1 y1 x2 y2@ represents rectangle region @[y1 .. y2] × [x1 .. x2]@
data Rect = Rect
  { rectLeft :: !Int,
    rectTop :: !Int,
    rectRight :: !Int,
    rectBottom :: !Int
  }
  deriving (Show, Read, Eq, Ord)

rectWidth, rectHeight :: Rect -> Int
rectWidth r = rectRight r - rectLeft r + 1
rectHeight r = rectBottom r - rectTop r + 1

xRange, yRange :: Rect -> Range
xRange (Rect x1 _ x2 _) = Range x1 x2
yRange (Rect _ y1 _ y2) = Range y1 y2

rangesToRect :: Range -> Range -> Rect
rangesToRect (Range x1 x2) (Range y1 y2) = Rect x1 y1 x2 y2

rectUnion :: Rect -> Rect -> Rect
rectUnion
  (Rect x1a y1a x2a y2a)
  (Rect x1b y1b x2b y2b) =
    Rect
      (min x1a x1b)
      (min y1a y1b)
      (max x2a x2b)
      (max y2a y2b)

rectIntersection :: Rect -> Rect -> Maybe Rect
rectIntersection r r' =
  rangesToRect
    <$> intersection (xRange r) (xRange r')
    <*> intersection (yRange r) (yRange r')

instance ShapeCompare Rect where
    shapeRel :: Rect -> Rect -> ShapeRel
    shapeRel (Rect x1 y1 x2 y2) (Rect x1' y1' x2' y2') = relX <> relY
      where
        relX = shapeRel (Range x1 x2) (Range x1' x2')
        relY = shapeRel (Range y1 y2) (Range y1' y2')

instance ShapeLattice Rect where
  union = rectUnion
  intersection = rectIntersection

-- * BoundingBox

class BoundingBox s where
    boundingBox :: s -> Rect

instance BoundingBox Rect where
    boundingBox = id

instance BoundingBox Point where
    boundingBox (Point x y) = Rect x y x y

-- instances

instance Formattable Rect where
  formatPrec _ (Rect l t r b) =
    pretty "Rect"
      <> encloseSep
        lbrace
        rbrace
        comma
        [ pretty "l=" <> formatDoc l,
          pretty "t=" <> formatDoc t,
          pretty "r=" <> formatDoc r,
          pretty "b=" <> formatDoc b
        ]