{-# LANGUAGE InstanceSigs #-}
module Geom
  ( -- * Point
    Point (..),

    -- * Range
    Range (..),

    -- * Relations
    ShapeCompare (..),
    ShapeRel (..),
    isContains,
    isContained,
    isIntersects,
    
    -- * Lattice operations
    ShapeLattice(..),
    shapeUnions, shapeIntersections
  )
where

import Data.List (foldl')
import Text.PrettyPrint.Formattable
import Text.PrettyPrint.Util

--------------------------------------------------------
--  Geometry Types
--------------------------------------------------------

-- | Point x y
data Point = Point !Int !Int
  deriving (Show, Read, Eq, Ord)

instance ShapeCompare Point where
  shapeRel p1 p2 = if p1 == p2 then Equivalent else Disjoint

-- | Range x1 x2 means [x1..x2]
data Range = Range !Int !Int
  deriving (Show, Read, Eq, Ord)

instance ShapeCompare Range where
    shapeRel :: Range -> Range -> ShapeRel
    shapeRel r1 r2 = case intersection r1 r2 of
        Nothing -> Disjoint
        Just rInt -> case (rInt == r2, rInt == r1) of
            (True, True) -> Equivalent
            (True, False) -> StrictContains
            (False, True) -> StrictContained
            (False, False) -> StrictIntersects

instance ShapeLattice Range where
    union (Range a1 a2) (Range b1 b2) = Range (min a1 b1) (max a2 b2)

    intersection (Range a1 a2) (Range b1 b2)
        | c1 <= c2 = Just (Range c1 c2)
        | otherwise = Nothing
      where c1 = max a1 b1
            c2 = min a2 b2

-- * Shape relations

class Eq s => ShapeCompare s where
  shapeRel :: s -> s -> ShapeRel

  intersects :: s -> s -> Bool
  intersects x y = shapeRel x y /= Disjoint

  contains :: s -> s -> Bool
  contains x y = isContains (shapeRel x y)

-- | Relation of two shapes as non-empty sets of points in the space.
--   They are mutually exclusive conditions between two **non-empty** sets @X, Y@.
--
-- * @Equivalent@: @X = Y@
-- * @StrictContains@: @X ⊋ Y@
-- * @StrictContained@: @X ⊊ Y@
-- * @StrictIntersects@: @X ≠ Y@ and @X ∩ Y ≠ ∅@
-- * @Disjoint@: @X ∩ Y = ∅@
--
-- @ShapeRel@ is a @Monoid@ (actually meet-semilattice) by meet ("min") operation on the
-- following order.
--
-- >           Equivalent
-- >             /    \
-- >  StrictContains  StrictContained
-- >            \      /
-- >         StrictIntersects
-- >                |
-- >            Disjoint
-- 
-- If @R1@ is the relation between shapes @X1, Y1@ in a space @U1@ and
-- @R2@ is the relation between @X2, Y2@ in a space @U2@, @R1 <> R2@ is the relation of
-- @X1 × X2@ and @Y1 × Y2@ in the space @U1 × U2@.
data ShapeRel = Equivalent | StrictContains | StrictContained | StrictIntersects | Disjoint
  deriving (Show, Read, Eq, Ord, Enum, Bounded)

isContains, isContained :: ShapeRel -> Bool
isContains rel = rel == Equivalent || rel == StrictContains
isContained rel = rel == Equivalent || rel == StrictContained

isIntersects :: ShapeRel -> Bool
isIntersects = (/= Disjoint)

instance Semigroup ShapeRel where
  Disjoint <> _ = Disjoint
  _ <> Disjoint = Disjoint
  StrictIntersects <> _ = StrictIntersects
  _ <> StrictIntersects = StrictIntersects
  StrictContains <> StrictContained = StrictIntersects
  StrictContains <> _relY           = StrictContains
      {- because  (_relY == Equivalent || _relY == StrictContains) -}
  StrictContained <> StrictContains = StrictIntersects
  StrictContained <> _relY          = StrictContained
      {- because  (_relY == Equivalent || _relY == StrictContained) -}
  Equivalent <> relY = relY

instance Monoid ShapeRel where
  mempty = Equivalent

instance Formattable ShapeRel
-- * Pretty printing

instance Formattable Point where
  formatPrec _ (Point x y) = formatDoc (x,y)


instance Formattable Range where
  formatPrec _ (Range x1 x2) =
    brackets $ formatDoc x1 <> pretty ".." <> formatDoc x2

-- * Shape lattices

class ShapeLattice s where
  -- | @shapeUnion x y@ = minimum shape @z@ such that @z@ contains both @x@ and @y@
  union :: s -> s -> s
  -- | @shapeIntersection x y = Just z@ if @x@ and @y@ intersects, and @z@ is the maximum shape that is contained by both @x@ and @y@,
  --
  --   If @x@ and @y@ are disjoint, @shapeIntersection x y = Nothing@.
  intersection :: s -> s -> Maybe s

shapeUnions :: ShapeLattice s => [s] -> s
shapeUnions [] = error "shapeUnions []"
shapeUnions (s : ss) = foldl' union s ss

shapeIntersections :: ShapeLattice s => [s] -> Maybe s
shapeIntersections [] = error "shapeIntersections []"
shapeIntersections (s : ss) = go s ss
  where
    go x [] = Just x
    go x (y : xs) = intersection x y >>= \z -> go z xs