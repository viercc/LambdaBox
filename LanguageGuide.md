# LambdaBox Language Guide

LambdaBox is a very simple and primitive graphical programming language
which is pure and functional.

In LambdaBox, you write an expression of lambda calculus extended with `letrec`, in an unusual way.
You write that in the form of a *bitmap image*!

To interpret that lambda calculus expression as a program,
LambdaBox uses the convention as [LazyK](https://tromp.github.io/cl/lazy-k.html) do.
In summary, each input and output is encoded to/decoded from the infinite list of church-encoded natural numbers.
An element of the infinite list represents a byte, and numbers greater than 255 represents the EOF.

This documentation focuses on how LambdaBox parses an image into lambda calculus expressions.

## Overview
A LambdaBox program is a bitmap image as said above. Specifically, it is a bitmap image
drawn by black (#000000) lines on backgrounds of non-black pixels.

Let's start with a simple example. The following image represents `\x -> x`, the identity function.

![first example: identity function](doc-img/id.png)  
(The image is scaled up to show. original is [this](example/id.png))

In LambdaBox, a box (rectangle) drawn by black lines represents either lambda abstraction (`\x -> ...`) or function application (`x y`) in lambda calculus.
They are differentiated by size of the box. A small, 3x3-sized box is function application and all other boxes are lambda abstraction.

Other lines which do not form rectangles are variables.
A line touching to the top line of a box (![top-T](doc-img/top-t.png)) is the variable bound by the lambda abstraction (`x` of `\x -> ...`).
Also, a line touches to the bottom line of a box (![bottom-T](doc-img/bottom-t.png)) is the variable the lambda abstraction returns.

You can now explain why the above image means `\x -> x`.
It is a large box, hence a lambda abstraction `\x -> ...`. The line in the middle represents the bound variable `x`, and this lambda returns that variable immediately.
It's `\x -> x`.

The second example is the `const` function, `\x -> \y -> x`.

![second example: const function](doc-img/const.png)  
(The image is scaled up to show. original is [this](example/const.png))

To interpret this program, you need two additional rules.

* If two lines cross, they are not connected each other.
* To assign a lambda abstraction value to a variable,
  connect a line which represents that variable to the outside of
  the box which represents lambda abstraction.

Step-by-step parsing of above program is like this:

1. We see the outermost box, so its `\x -> E`
2. Inside it, there is a box, so there's `\y -> F` in E.
3. Inside the inner box, it returns `x` from outside binding. So `F = x`.
4. The value of inner box (`\y -> x`) is assigned to another variable, `z`.
5. The outermost box returns `z`. So `E = z`.
6. Putting all together, it's `\x -> letrec z = (\y -> x) in z`.
7. It simplifies to `\x -> \y -> x`.

The third example uses function application, represented by a small 3x3 box.
To represent `z = x y`, a wire for the variable `x` connect from left, wire for `y` connect from top, and wire for `z` connect from bottom right to the box.
The picture below summarize the usage of function application.

![Diagram explains function application](doc-img/diagram-ap.png)

This is an example using function application. Let's walk through again.

![third example: function application](doc-img/revap.png)  
(The image is scaled up to show. original is [this](example/revap.png))

1. We see the outermost box, so its `\x -> E`
2. Inside it, there is a box, so there's `\y -> F` in E.
3. Inside inner box, there is a function application. It's "function" side is `y`
   (bound variable of inner lambda), "argument" side is `x` (bound variable of
   outer lambda).
   The result of the application is bound to another variable `z`.
4. Inner abstraction box returns `z`. so
   `F = y x`.
5. Outer box returns the variable which is assigned the value of the inner box.
   so `E = \y -> F`.
6. Putting all together, the whole program is `\x -> \y -> y x`.

Using these rules, you can construct arbitrary lambda expressions.

Final example is the successor function for
[Church encoded natural number](https://en.wikipedia.org/wiki/Church_encoding#Church_numerals):
`\n f x -> f (n f x)`.

![fourth example: successor function](doc-img/succ.png)  
(The image is scaled up to show. original is [this](example/succ.png))

Can you parse it?

## Detailed explanation of parsing process

### Preprocess

As the first step of parsing source image, LambdaBox preprocesses any image into a binary image.

1. Convert input image to RGB image of 8bit-depth per channel.
2. Each pixel is converted to either Black or White. Only #000000 pixels are Black,
   Other pixels are White.

You can use any non-#000000 colored pixels to draw "comments" in the source image.

See the following program:

![Preprocess example input](example/hello.png)

This is the preprocessed image:

![Preprocess example output](doc-img/hello-nocomment.png)

### Break down binary image to set of lines

The second step of the parsing is to break down binary images to the set of lines.
Here, a "line" means a consecutive block of black pixels aligned horizontally or vertically. a line must have 2 or more length.

**Example:**

Below shows an image containing 5 lines.

![Lines example](doc-img/explain-lines.png)

This image contains no line; lines are either horizontal or vertical.

![Lines example 2](doc-img/no-diag-line.png)

### Construct Boxes and Wires from set of lines

If four lines are touching each other at their endpoints and forms a cycle,
it must be a rectangle. We call it a box.

Lines do not form boxes are decomposed into connected components. If a line which does not form a box touches another line, it is connected to that line, unless they cross each other.
Each such connected component is a wire.

A box which is of size 3x3 is an application box. If it is larger than 3x3, it's an abstraction box. If it has 2 or shorter lengthed edge, it is an invalid LambdaBox program.

A wire must have no loop in it. If it has, it is invalid.

**Example:**

Left of this image contains two wires and no boxes. Right side explains it.

![Wires example](doc-img/explain-wire.png)

Left of this image contains two wires and two boxes.

Note that wire can be connected to an edge of boxes. But if a line of a wire *crosses* an edge of a box, they are not considered connected. Connections between wires and boxes matters in the next step.

![Wires example 2](doc-img/explain-wire2.png)

### Convert Boxes & Wires to Lambda calculus expression

Name each box and wire a unique identifier. These identifiers are used as variable names.

Construct a tree of the boxes. A box is a child of another box if and only if it is contained in the other box completely and there is no other box between them.

Contains-Contained relation on boxes are not always a tree on arbitrarily placed boxes.
There are two restrictions about placement of boxes for a LambdaBox program.

1. There should be the 'toplevel' box containing all other boxes.
2. There should be no two overlapping boxes which are not in the relation one box contains the other box.

A LambdaBox program is converted to lambda expression recursively, starting from the 'toplevel' box.

The converted program has the following form.

```
letrec {
  ...
} in box_toplevel
```

Here, `letrec` is similar to Haskell's `let`, which is lazy and mutually recursive
binding of values to variables.
This `...` is filled with conversion result of the toplevel box.

To convert an application box with these parameters,

* Identifier of the box: `i`
* Identifier of a wire connected to the left line of this box from outside: `fun`
* Identifier of a wire connected to the top line of this box from outside: `arg`
* Identifier of wires connected to the bottom or right line of this box from outside: `out1`, `out2`

It is converted to the following series of binding:
```
box_i = fun arg;
out1 = box_i;
out2 = box_i;
```

To convert an abstraction box with these parameters,

* Identifier of the box: `i`
* Identifier of children of the box: `j1`, `j2`, ...
* Identifier of wires connected to this box from outside: `out1`, `out2`, ...
* Identifier of wires connected to the top line of this box from inside: `arg1`, `arg2`, ...
* Identifier of the wire connected to the bottom line of this box from inside: `ret`

It is converted to the following series of binding, using the conversion results of
the children boxes.

```
box_i = \x_i ->
   letrec {
     arg1 = x_i;
     arg2 = x_i;
       :
     argN = x_i;
     (conversion results of box j1);
     (conversion results of box j2);
       :
   } in ret;
out1 = box_i;
out2 = box_i;
  :
outM = box_i;
```

### Final check

Expressions parsed from images by the above processes can be invalid from various reason.
A valid LambdaBox must not contain any of the following errors on the parsed expression.

1. Variables with no definitions
2. Variables with multiple definitions
3. Variables escaping their scope
